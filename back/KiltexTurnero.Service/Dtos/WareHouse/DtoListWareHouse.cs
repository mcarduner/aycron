﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AycronTest.Service.Dtos.WareHouse
{
    public class DtoListWareHouse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string County { get; set; }
        public string Zip { get; set; }      
        public string State { get; set; }

        public double Distance { get; set; }

        public DtoGeo Geo { get; set; }
    }
}
