﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AycronTest.Service.Dtos.WareHouse
{
    public class DtoGeo
    {
        public double lat { get; set; }

        public double lng { get; set; }
    }
}
