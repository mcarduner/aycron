﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AycronTest.Service.Dtos.WareHouse
{
    public class DtoLocationWareHouse
    {
        public int Count { get; set; }

        public DtoGeo Geo { get; set; }
    }
}
