﻿namespace AycronTest.Service.Dtos.WareHouse
{
    public class DtoAddWareHouse
    {

        public string Address { get; set; }

        public string Code { get; set; }

        public string Country { get; set; }

        public string County { get; set; }

        public DtoGeo Geo { get; set; }

        public string Name { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }
    }


}
