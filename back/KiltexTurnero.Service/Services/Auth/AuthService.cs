﻿using KiltexTurnero.Domain;
using KiltexTurnero.Domain.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Auth
{
    public class AuthService
    {
        private readonly SQLContext _context;
        
        public AuthService(SQLContext sqlContext)
        {
            _context = sqlContext;            
        }

        public async Task<OperationResponse<UserModel>> GetInternalUserLogin(string name, string password)
        {
            var user = await _context
                                    .Users
                                    .Include("Rol")
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync(u => u.Name == name && u.Password == password)
                                    .ConfigureAwait(false);

            return new OperationResponse<UserModel>(user,user != null);
        }

    }
}
