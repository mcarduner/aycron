﻿using KiltexTurnero.Service.OperationResponse;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Admin
{
    public class EmailService
    {
        private readonly string _apiKey;

        public EmailService(IConfiguration config)
        {
            _apiKey = config.GetValue<String>("SendGrid:APIKey");
        }

        // Punto único de envío de mails.
        private async Task<OperationResponse<string>> SendEmail(string userEmail, string userFullName, string subject, string htmlBody, string plainBody = "")
        {
            var client = new SendGridClient(_apiKey);

            var from = new EmailAddress("turnero.ktx@gmail.com", "Turnero Padel testing");
            var to = new EmailAddress(userEmail, userFullName);
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainBody, htmlBody);
            var response = await client.SendEmailAsync(msg);

            return new OperationResponse<string>(response.ToString(), response.IsSuccessStatusCode, null);
        }

        /// <summary>
        /// Notificación estado de reserva (Client)
        /// </summary>
        /// <param name="clientName"></param>
        /// <param name="clientEmail"></param>
        /// <param name="schedule"></param>
        /// <param name="court"></param>
        /// <param name="reservationStatus"></param>
        /// <returns></returns>
        public async Task<OperationResponse<string>> ReservationNotification(string clientName, string clientEmail, string turn, string court, string reservationStatus)
        {
            var emailBody = "Hola @@clientName@@, le informamos que su reserva del día @@schedule@@, en la cancha @@court@@ se encuentra en estado @@reservationStatus@@.";

            emailBody = emailBody.Replace("@@clientName@@", clientName)
                                 .Replace("@@turn@@", turn)
                                 .Replace("@@court@@", court)
                                 .Replace("@@reservationStatus@@", reservationStatus);

            return await SendEmail(clientEmail, clientName, "Reserva Padel", emailBody);
        }

        /// <summary>
        /// Solicitud de Reestablecimiento de Contraseña (Client)
        /// </summary>
        /// <param name="clientEmail"></param>
        /// <param name="token"></param>
        /// <param name="clientName"></param>
        /// <returns></returns>
        public async Task<OperationResponse<string>> SendClientPasswordResetRequestEmail(string clientEmail, string clientName, string token)
        {
            var emailBody = "Hola @@clientName@@, ingrese al siguiente link para reestablecer su contraseña. @@token@@";

            emailBody = emailBody.Replace("@@clientName@@", clientName)
                       .Replace("@@token@@", token);

            return await SendEmail(clientEmail, clientName, "Solicitud de Reestablecimiento de Contraseña", emailBody);
        }

        /// <summary>
        /// Notificación sobre cambio de contraseña de Clientes (aviso de seguridad)
        /// </summary>
        /// <param name="clientEmail"></param>
        /// <param name="clientName"></param>
        /// <returns></returns>
        public async Task<OperationResponse<string>> ClientPasswordChangeNotification(string clientEmail, string clientName)
        {
            var emailBody = "Hola @@clientName@@, su contraseña ha sido reestablecida con éxito.";

            emailBody = emailBody.Replace("@@clientName@@", clientName);

            return await SendEmail(clientEmail, clientName, "Reestablecimiento de Contraseña", emailBody);
        }
    }
}
