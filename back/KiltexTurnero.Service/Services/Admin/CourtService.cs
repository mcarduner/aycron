﻿using KiltexTurnero.Domain;
using KiltexTurnero.Domain.Models;
using KiltexTurnero.Service.Dtos.Court;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Admin
{
    public class CourtService
    {
        internal readonly SQLContext _contextMySql;

        public CourtService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<DtoGetCourt>> GetById(long id)
        {
            Court court = await _contextMySql
                        .Courts
                        .AsNoTracking()
                        .FirstOrDefaultAsync(c => c.Id == id)
                        .ConfigureAwait(false);

            // Comprobación: si la cancha existe
            if (court == null)
            {
                return new OperationResponse<DtoGetCourt>(null, false, "La cancha ingresada no existe");
            }

            var dto = new DtoGetCourt()
            {
                Id = court.Id,
                Name = court.Name,
                Description = court.Description,
                IsEnabled = court.IsEnabled,
                ImageUrl = court.ImageUrl,
            };
            return new OperationResponse<DtoGetCourt>(dto);
        }



        public async Task<OperationResponse<long>> Post(DtoAddCourt createrequest)
        {
            #region Comprobaciones
            // Si existe esa cancha.
            if (await _contextMySql.Courts.AnyAsync(c => c.Name == createrequest.Name && !c.IsDeleted))
            {
                return new OperationResponse<long>(0, false, "Ya existe una cancha registrada con ése nombre");
            }
            #endregion

            Court newCourt = new()
            {
                Name = createrequest.Name,
                Description = createrequest.Description,
                IsEnabled = createrequest.IsEnabled,
                ImageUrl = createrequest.ImageUrl,
            };

            var court = await _contextMySql.Courts.AddAsync(newCourt);
            await _contextMySql.SaveChangesAsync();

            return new OperationResponse<long>(court.Entity.Id);
        }

        public async Task<OperationResponse<long>> Update(DtoEditCourt updaterequest)
        {
            #region Comprobaciones
            // Si existe esa cancha.
            if (await _contextMySql.Courts.AnyAsync(c => c.Name == updaterequest.Name && !c.IsDeleted))
            {
                return new OperationResponse<long>(0, false, "Ya existe una cancha registrada con ése nombre");
            }
            #endregion

            Court court = await _contextMySql
                                    .Courts
                                    .FirstOrDefaultAsync(c => c.Id == updaterequest.Id && !c.IsDeleted);

            if (court == null) return new OperationResponse<long>(updaterequest.Id, false);

            court.Id = updaterequest.Id;
            court.Name = updaterequest.Name;
            court.Description = updaterequest.Description;
            court.IsEnabled = updaterequest.IsEnabled;
            court.ImageUrl = updaterequest.ImageUrl;

            _contextMySql.Courts.Update(court);
            await _contextMySql.SaveChangesAsync();

            return new OperationResponse<long>(updaterequest.Id);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoListCourt>>> ListCourts(string filter, int pageSize, int page, CancellationToken ct = default)
        {
            var query = _contextMySql
                                  .Courts
                                  .AsNoTracking()
                                  .Where(c => !c.IsDeleted && c.Name.ToLower().Contains(filter ?? ""));

            var count = await query.CountAsync().ConfigureAwait(false);

            var list = (await query.OrderBy(p => p.Name)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .ToListAsync(ct)
                                  .ConfigureAwait(false))
                                  .Select(c => new DtoListCourt()
                                  {
                                      Id = c.Id,
                                      Name = c.Name,
                                      Description = c.Description,
                                      IsEnabled = c.IsEnabled,
                                      ImageUrl = c.ImageUrl,
                                  });

            return new OperationResponse<DtoResponsePagination<DtoListCourt>>(new DtoResponsePagination<DtoListCourt>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

        public async Task<OperationResponse<long>> Delete(long id, CancellationToken ct = default)
        {
            Court court = await _contextMySql
                                         .Courts
                                         .FirstOrDefaultAsync(p => p.Id == id && !p.IsDeleted, ct)
                                         .ConfigureAwait(false);
            if (court != null)
            {
                court.IsDeleted = true;
                await _contextMySql.SaveChangesAsync(ct).ConfigureAwait(false);
            }

            return new OperationResponse<long>(court.Id);
        }

    }
}
