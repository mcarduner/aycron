﻿using KiltexTurnero.Domain;
using KiltexTurnero.Domain.Models;
using KiltexTurnero.Service.Dtos.Client;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Admin
{
    public class ClientService
    {
        internal readonly SQLContext _contextMySql;

        public ClientService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<DtoGetClient>> GetById(long id)
        {
            Domain.Models.Client client = await _contextMySql
                                .Clients
                                .AsNoTracking()
                                .FirstOrDefaultAsync(c => c.Id == id)
                                .ConfigureAwait(false);

            // Comprobación: si el cliente existe
            if (client == null)
            {
                return new OperationResponse<DtoGetClient>(null, false, "El cliente ingresado no existe");
            }

            var dto = new DtoGetClient()
            {
                Id = client.Id,
                FirstName = client.FirstName,
                LastName = client.LastName,
                EmailAddress = client.EmailAddress,
                IsRegistered = client.IsRegistered ? "SI" : "NO"
            };
            return new OperationResponse<DtoGetClient>(dto);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoListClient>>> ListClients(string filter, int pageSize, int page, CancellationToken ct = default)
        {
            var query = _contextMySql
                                .Clients
                                .AsNoTracking()
                                .Where(p => !p.IsDeleted && (p.FirstName.ToLower().Contains(filter ?? "") ||
                                                             p.LastName.ToLower().Contains(filter ?? "") ||
                                                             p.IsRegistered.ToString().ToLower().Contains(filter ?? "")));

            var count = await query.CountAsync(ct).ConfigureAwait(false);

            var list = (await query.OrderBy(p => p.FirstName + p.LastName + p.IsRegistered)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .ToListAsync(ct)
                                  .ConfigureAwait(false))
                                  .Select(c => new DtoListClient()
                                  {
                                      Id = c.Id,
                                      FirstName = c.FirstName,
                                      LastName = c.LastName,
                                      EmailAddress = c.EmailAddress,
                                      IsRegistered = c.IsRegistered ? "SI" : "NO"
                                  });

            return new OperationResponse<DtoResponsePagination<DtoListClient>>(new DtoResponsePagination<DtoListClient>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

    }
}
