﻿using KiltexTurnero.Domain;
using KiltexTurnero.Domain.Models;
using KiltexTurnero.Service.Dtos.Schedule;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Admin
{
    public class ScheduleService
    {
        internal readonly SQLContext _contextMySql;
        public ScheduleService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<List<DtoGetSchedule>>> GetAll()
        {
            List<Schedule> getAllSchedule = await _contextMySql.Schedules.ToListAsync().ConfigureAwait(false);

            List<DtoGetSchedule> schedules = new List<DtoGetSchedule>();

            foreach (var item in getAllSchedule)
            {

                var dto = new DtoGetSchedule()
                {
                    Id = item.Id,
                    StartDate = item.StartDate.ToLocalTime(),
                    EndDate = item.EndDate.ToLocalTime(),
                };

                schedules.Add(dto);

            }

            return new OperationResponse<List<DtoGetSchedule>>(schedules);
        }

        public async Task<OperationResponse<DtoGetSchedule>> GetById(long id)
        {
            Schedule turn = await _contextMySql
                                .Schedules
                                .AsNoTracking()
                                .FirstOrDefaultAsync(s => s.Id == id)
                                .ConfigureAwait(false);

            // Comprobación: si el turno existe
            if (turn == null)
            {
                return new OperationResponse<DtoGetSchedule>(null, false, "El turno ingresado no existe");
            }

            var dto = new DtoGetSchedule()
            {
                Id = turn.Id,
                StartDate = turn.StartDate.ToLocalTime(),
                EndDate = turn.EndDate.ToLocalTime(),
            };
            return new OperationResponse<DtoGetSchedule>(dto);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoGetScheduleByCourt>>> GetScheduleByIdCourt(string filter, int pageSize, int page, long idCourt,  CancellationToken ct = default)
        {
            var query = _contextMySql.Schedules
                            .AsNoTracking()
                            .Where(c => !c.IsDeleted && c.Id.ToString().ToLower().Contains(filter ?? "") && c.IdCourt == idCourt);

            var count = await query.CountAsync().ConfigureAwait(false);

            var list = (await query.OrderBy(p => p.Id)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .ToListAsync(ct)
                                  .ConfigureAwait(false))
                                  .Select(s => new DtoGetScheduleByCourt()
                                  {
                                      Id = s.Id,
                                      IdCourt = idCourt,
                                      StartDate = s.StartDate.ToLocalTime(),
                                      EndDate = s.EndDate.ToLocalTime(),
                                      Name = s.Name,
                                  });

            return new OperationResponse<DtoResponsePagination<DtoGetScheduleByCourt>>(new DtoResponsePagination<DtoGetScheduleByCourt>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });

        }


        public async Task<OperationResponse<long>> Post(DtoAddSchedule createrequest)
        {
      
            #region Comprobaciones
            // Si existe ese turno.
            if (await _contextMySql.Schedules.AnyAsync(s => s.Id == createrequest.Id && !s.IsDeleted))
            {
                return new OperationResponse<long>(0, false, "Ya existe ese turno");
            }

            // Si existe un turno con ese horario.
            if (await _contextMySql.Schedules.AnyAsync(s => s.Id != createrequest.Id && s.StartDate == createrequest.StartDate && s.EndDate == createrequest.EndDate && !s.IsDeleted))
            {
                return new OperationResponse<long>(0, false, "Ya existe un turno con ese horario");
            }

            // Que el turno sea posterior a fecha actual
            if (createrequest.StartDate < DateTime.Now.ToLocalTime() && createrequest.EndDate < DateTime.Now.ToLocalTime())
            {
                return new OperationResponse<long>(0, false, "El horario del turno debe ser posterior a la hora actual");
            }
            #endregion
            
            Schedule newSchedule = new()
            {
                Id = createrequest.Id,
                StartDate = createrequest.StartDate.ToLocalTime(),
                EndDate = createrequest.EndDate.ToLocalTime(),
                Name = createrequest.Name,
                State = createrequest.State,
                Duration = createrequest.Duration,
                IdCourt = createrequest.IdCourt,
            };

            for (DateTime i = newSchedule.StartDate.Date; i <= newSchedule.EndDate.Date; i = i.AddDays(1))
            {
                for (TimeSpan j = newSchedule.StartDate.TimeOfDay; j < newSchedule.EndDate.TimeOfDay; j = j.Add(TimeSpan.FromMinutes(newSchedule.Duration)))
                {
                    Turn turn = new()
                    {
                        Disabled = false,
                        StartTime = i.AddTicks(j.Ticks).ToLocalTime(),
                        EndTime = i.AddTicks(j.Add(TimeSpan.FromMinutes(newSchedule.Duration)).Ticks).ToLocalTime(),
                        ScheduleId = newSchedule.Id,
                        State = true,
                        IsDeleted = false
                    };
                    newSchedule.Turn.Add(turn);
                }
            }

            var schedule = await _contextMySql.Schedules.AddAsync(newSchedule);
            await _contextMySql.SaveChangesAsync();

            return new OperationResponse<long>(schedule.Entity.Id);
        }

        

        public async Task<OperationResponse<DtoResponsePagination<DtoListSchedule>>> ListSchedules(string filter, int pageSize, int page, CancellationToken ct = default)
        {
            var query = _contextMySql
                                  .Schedules
                                  .AsNoTracking()
                                  .Where(c => !c.IsDeleted && c.Id.ToString().ToLower().Contains(filter ?? ""));

            var count = await query.CountAsync().ConfigureAwait(false);

            var list = (await query.OrderBy(p => p.Id)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .ToListAsync(ct)
                                  .ConfigureAwait(false))
                                  .Select(s => new DtoListSchedule()
                                  {
                                      Id = s.Id,
                                      StartDate = s.StartDate.ToLocalTime(),
                                      EndDate = s.EndDate.ToLocalTime(),
                                  });

            return new OperationResponse<DtoResponsePagination<DtoListSchedule>>(new DtoResponsePagination<DtoListSchedule>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

        

        public async Task<OperationResponse<long>> Delete(long id, CancellationToken ct = default)
        {
            Schedule schedule = await _contextMySql
                                         .Schedules
                                         .FirstOrDefaultAsync(s => s.Id == id && !s.IsDeleted, ct)
                                         .ConfigureAwait(false);
            if (schedule != null)
            {
                schedule.IsDeleted = true;
                await _contextMySql.SaveChangesAsync(ct).ConfigureAwait(false);
            }

            return new OperationResponse<long>(schedule.Id);
        }

    }
}
