﻿using KiltexTurnero.Domain;
using KiltexTurnero.Service.Dtos.Media;
using KiltexTurnero.Service.OperationResponse;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Admin
{
    public class MediaService
    {
        internal readonly SQLContext _contextMySql;
        public MediaService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<dynamic>> UploadMedia(DtoUploadMedia request, string root)
        {
            string[] allowedImageFormat = { "jpg", "jpeg", "png" };

            // Comprobaciones de formato
            if (!request.IsPdf() && !allowedImageFormat.Contains(request.GetExtension()))
            {
                return new OperationResponse<dynamic>("", false, "El formato seleccionado es incorrecto. Seleccione una imagen con formato JPG, JPEG o PNG");
            }

            // Establecemos ruta (path) y nombre de archivo
            string fileName = $"{DateTime.Now:yyyy-MM-dd_HH_mm}-{DateTime.Now.Ticks}_{request.Name}";
            string completePath = Path.Combine(root, request.Folder == "certificate" ? "PrivateFiles" : "wwwroot", "media", request.Folder, fileName);

            using Stream file = File.Create(completePath);
            await request.File.CopyToAsync(file);

            request.File.Dispose();

            (string iud, string imageName) res = (Guid.NewGuid().ToString().Replace("-", ""), fileName);

            return new OperationResponse<dynamic>(new { uid = res.iud, name = res.imageName });
        }

    }
}
