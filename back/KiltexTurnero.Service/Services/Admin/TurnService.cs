﻿using KiltexTurnero.Domain;
using KiltexTurnero.Service.Dtos.Turn;
using KiltexTurnero.Service.OperationResponse;
using System.Threading.Tasks;
using KiltexTurnero.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using KiltexTurnero.Service.Models;
using System.Threading;
using System;
using System.Collections.Generic;
using KiltexTurnero.Service.Dtos.Court;

namespace KiltexTurnero.Service.Services.Admin
{
    public class TurnService
    {
        internal readonly SQLContext sqlContext;

        public TurnService(SQLContext sqlContext)
        {
            this.sqlContext = sqlContext;
        }
        public async Task<OperationResponse<long>> Post( TurnAddDto createRequest )
        {
            Turn newTurn = new()
            {
                Id = createRequest.Id,
                StartTime = createRequest.StartTime.ToLocalTime(),
                EndTime = createRequest.EndTime.ToLocalTime(),
                State = createRequest.State,
                Disabled = createRequest.Disabled,
                ScheduleId = createRequest.ScheduleId,

            };

            var turn = await sqlContext.Turn.AddAsync(newTurn);
            await sqlContext.SaveChangesAsync();

            return new OperationResponse<long>(turn.Entity.Id);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoGetAvailableTurns>>> GetAvailableTurns(int pageSize, int page, DateTime fecha, long idCourt, CancellationToken ct = default)
        {
            var query = sqlContext.Turn
                        .AsNoTracking()
                        .Where(t => t.State && !t.IsDeleted && (t.StartTime.Date == fecha) && t.Schedule.IdCourt == idCourt);

            var count = await query.CountAsync().ConfigureAwait(false);

            var list = (await query
                        .OrderBy(p => p.Id)
                        .Skip(page * pageSize)
                        .Take(pageSize)
                        .ToListAsync(ct)
                        .ConfigureAwait(false))
                        .Select(t => new DtoGetAvailableTurns()
                        {
                            Id = t.Id,
                            StartTime = t.StartTime.ToString(),
                            EndTime = t.EndTime.ToString(),
                        });

            return new OperationResponse<DtoResponsePagination<DtoGetAvailableTurns>>(new DtoResponsePagination<DtoGetAvailableTurns>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

        public async Task<OperationResponse<DtoResponsePagination<TurnGetByScheduleDto>>> GetByScheduleId(long ScheduleId, string filter, int pageSize, int page, CancellationToken ct = default)
        {
            var query =sqlContext
                      .Turn
                      .AsNoTracking()
                      .Where(c => !c.IsDeleted && c.Id.ToString().ToLower().Contains(filter ?? "") && c.ScheduleId == ScheduleId);

            var count = await query.CountAsync().ConfigureAwait(false);

            var list = (await query.OrderBy(p => p.Id)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .ToListAsync(ct)
                                  .ConfigureAwait(false))
                                  .Select(t => new TurnGetByScheduleDto()
                                  {
                                      Id = t.Id,
                                      StartTime = t.StartTime.ToString(),
                                      EndTime = t.EndTime.ToString(),
                                      ScheduleId = t.ScheduleId,
                                      State = t.State,  
                                  });

            return new OperationResponse<DtoResponsePagination<TurnGetByScheduleDto>>(new DtoResponsePagination<TurnGetByScheduleDto>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

        public async Task<OperationResponse<DtoGetTurnById>> GetTurnById(long id)
        {
            Turn turnId = await sqlContext
                          .Turn
                          .AsNoTracking()
                          .FirstOrDefaultAsync(c => c.Id == id)
                          .ConfigureAwait(false);

          
            var dto = new DtoGetTurnById()
            {
                StartTime = turnId.StartTime.ToString(),
                EndTime = turnId.EndTime.ToString(),
                State = turnId.State,
            };
            return new OperationResponse<DtoGetTurnById>(dto);
        }

        public async Task<OperationResponse<long>> Update(DtoEditTurn request)
        {

            var query = await sqlContext.Turn
                        .FirstOrDefaultAsync(c => c.Id == request.Id && !c.IsDeleted);

            query.Id = request.Id;
            query.State = request.State;

            sqlContext.Update(query);
            await sqlContext.SaveChangesAsync();

            return new OperationResponse<long>(request.Id);
        }

        public async Task<OperationResponse<long>> Delete(long id , CancellationToken ct = default)
        {
            Turn turn = await sqlContext
                              .Turn
                              .FirstOrDefaultAsync(c => c.Id == id && !c.IsDeleted)
                              .ConfigureAwait(false);

            if (turn != null)
            {
                turn.IsDeleted = true;
                await sqlContext.SaveChangesAsync(ct).ConfigureAwait(false);
            }

            return new OperationResponse<long>(turn.Id);
        } 

    }
}
