﻿using KiltexTurnero.Domain;
using KiltexTurnero.Domain.Models;
using KiltexTurnero.Service.Dtos.Schedule;
using KiltexTurnero.Service.Dtos.SystemParameter;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Admin
{
    public class SystemParameterService
    {
        internal readonly SQLContext _contextMySql;

        public SystemParameterService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<DtoGetSystemParameter>> GetById(long id)
        {
            SystemParameter sParameter = await _contextMySql
                                    .SystemParameters
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync(sp => sp.Id == id)
                                    .ConfigureAwait(false);

            if (sParameter == null) return new OperationResponse<DtoGetSystemParameter>(null, false, "Parámetro no encontrado");

            var dto = new DtoGetSystemParameter()
            {
                Id = id,
                Key = sParameter.Key,
                Value = sParameter.Value,
                Description = sParameter.Description
            };

            return new OperationResponse<DtoGetSystemParameter>(dto);
        }

        public async Task<OperationResponse<long>> Post(DtoAddSystemParameter createrequest)
        {
            // Si existe ese parametro.
            if (await _contextMySql.SystemParameters.AnyAsync(sp => sp.Id == createrequest.Id && !sp.IsDeleted))
            {
                return new OperationResponse<long>(0, false, "Ya existe ese parametro");
            }

            // Si existe un parametro con esa Key.
            if (await _contextMySql.SystemParameters.AnyAsync(sp => sp.Key == createrequest.Key && !sp.IsDeleted))
            {
                return new OperationResponse<long>(0, false, "Ya existe esa key");
            }

            SystemParameter newParameter = new()
            {
                Id = createrequest.Id,
                Key = createrequest.Key,
                Value = createrequest.Value,
                Description = createrequest.Description,
            };

            var sParameter = await _contextMySql.SystemParameters.AddAsync(newParameter);
            await _contextMySql.SaveChangesAsync();

            return new OperationResponse<long>(sParameter.Entity.Id);
        }

        public async Task<OperationResponse<long>> Update(DtoEditSystemParameter updaterequest, long userId)
        {
            SystemParameter sParameter = await _contextMySql
                                                .SystemParameters
                                                .FirstOrDefaultAsync(p => p.Id == updaterequest.Id);

            if (sParameter == null) return new OperationResponse<long>(0, false, "Parametro no encontrado");

            // Si ya existe un parametro con esa Key.
            if (await _contextMySql.SystemParameters.AnyAsync(sp => sp.Key == updaterequest.Key && !sp.IsDeleted))
            {
                return new OperationResponse<long>(0, false, "Ya existe un parametro con esa key");
            }

            var user = await _contextMySql
                                .InternalUsers
                                .FirstOrDefaultAsync(p => p.Id == userId);

            sParameter.Key = updaterequest.Key;
            sParameter.Value = updaterequest.Value;
            sParameter.Description = updaterequest.Description;

            _contextMySql.SystemParameters.Update(sParameter);
            await _contextMySql.SaveChangesAsync();

            return new OperationResponse<long>(updaterequest.Id);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoListSystemParameter>>> ListSystemParameter(string filter, int pageSize, int page, CancellationToken ct = default)
        {
            var query = _contextMySql
                                    .SystemParameters
                                    .AsNoTracking()
                                    .Where(sp => sp.Key.ToLower().Contains(filter ?? ""));

            var count = await query.CountAsync().ConfigureAwait(false);

            var list = await query.OrderBy(sp => sp.Key)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .Select(sp => new DtoListSystemParameter()
                                  {
                                      Id = sp.Id,
                                      Key = sp.Key,
                                      Value = sp.Value,
                                      Description = sp.Description,
                                  }).ToListAsync()
                                  .ConfigureAwait(false);

            return new OperationResponse<DtoResponsePagination<DtoListSystemParameter>>(new DtoResponsePagination<DtoListSystemParameter>
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

        public async Task<OperationResponse<long>> Delete(long id, CancellationToken ct = default)
        {
            SystemParameter sParameter = await _contextMySql
                                         .SystemParameters
                                         .FirstOrDefaultAsync(s => s.Id == id && !s.IsDeleted, ct)
                                         .ConfigureAwait(false);
            
            if (sParameter != null)
            {
                sParameter.IsDeleted = true;
                await _contextMySql.SaveChangesAsync(ct).ConfigureAwait(false);
            }

            return new OperationResponse<long>(sParameter.Id);
        }
    }
}
