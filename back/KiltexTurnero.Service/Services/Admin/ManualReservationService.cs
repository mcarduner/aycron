﻿using KiltexTurnero.Cross.Enums;
using KiltexTurnero.Domain;
using KiltexTurnero.Domain.Models;
using KiltexTurnero.Service.Dtos.ManualReservation;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Admin
{
    public class ManualReservationService
    {
        internal readonly SQLContext _contextMySql;
        private EmailService _emailService;

        public ManualReservationService(SQLContext contextMySql, EmailService emailService)
        {
            _contextMySql = contextMySql;
            _emailService = emailService;
        }

        public async Task<OperationResponse<DtoGetManualReservation>> GetById(long id)
        {
            Reservation manualReservation = await _contextMySql
                        .Reservations
                        .AsNoTracking()
                        .Include(s => s.Client)
                        .Include(s => s.Court)
                        //.Include(s => s.Schedule)
                        .Include(s => s.PaymentMethod)
                        .Include(e => e.ReservationStatus)
                        .FirstOrDefaultAsync(c => c.Id == id)
                        .ConfigureAwait(false);

            // Comprobación: si la reserva existe
            if (manualReservation == null)
            {
                return new OperationResponse<DtoGetManualReservation>(null, false, "La reserva solicitada no existe. Ingrese un Id válido por favor.");
            }

            DtoGetManualReservation dto = new()
            {
                Id = manualReservation.Id,
                Timestamp = manualReservation.Timestamp,
                Client = $"{manualReservation.Client.FirstName} {manualReservation.Client.LastName}",
                Court = manualReservation.Court.Name,
                //Schedule = $"{manualReservation.Schedule.StartDate} {manualReservation.Schedule.EndDate}",
                PaymentMethod = manualReservation.PaymentMethod.Label,
                ReservationStatus = manualReservation.ReservationStatus.Label,
                IsRegistered = manualReservation.Client.IsRegistered ? "SI" : "NO"
            };
            return new OperationResponse<DtoGetManualReservation>(dto);
        }

        public async Task<OperationResponse<long>> AddManualReservation(DtoAddManualReservation createrequest, long internalUserID)
        {
            #region Comprobaciones
            // Si el Id corresponde al usuario logeado.
            if (!(await _contextMySql.InternalUsers.AnyAsync(u => u.Id == internalUserID && !u.IsDeleted)))
            {
                return new OperationResponse<long>(0, false, "Debes iniciar sesión o registrarte para realizar inscripciones.");
            }

            Domain.Models.Client client = await _contextMySql.Clients.FirstOrDefaultAsync(s => s.Id == createrequest.ClientId && !s.IsDeleted);

            // Si existe el cliente.
            if (client == null)
            {
                return new OperationResponse<long>(0, false, "El Id ingresado no corresponde a un cliente registrado.");
            }

            Court court = await _contextMySql.Courts.FirstOrDefaultAsync(c => c.Id == createrequest.CourtId && !c.IsDeleted);

            // Si existe la cancha para inscribirse y si está habilitada.
            if (court == null)
            {
                return new OperationResponse<long>(0, false, "La cancha no existe.");
            }

            if (!court.IsEnabled)
            {
                return new OperationResponse<long>(0, false, "La cancha está inhabilitada por el momento.");
            }

            Turn turn = await _contextMySql.Turn.FirstOrDefaultAsync(s => s.Id == createrequest.TurnId && !s.IsDeleted);

            // Si la cancha está abierta en el horario seleccionado
            //if (court.OpeningTime < schedule.StartDate || court.ClosingTime < schedule.EndDate)
            //{
            //    return new OperationResponse<long>(0, false, "Seleccione un horario disponible para ésta cancha.");
            //}

            // Si el cliente ya tiene una reserva en ése horario en ésa cancha.
            if (await _contextMySql.Reservations.AnyAsync(r => r.ClientId == createrequest.ClientId /*&& r.ScheduleId == createrequest.ScheduleId*/ && r.CourtId == createrequest.CourtId && !r.IsDeleted))
            {
                return new OperationResponse<long>(0, false, "El cliente seleccionado ya tiene una reserva en ése horario.");
            }

            // Si la cancha ya está ocupada en ese horario.
            if (await _contextMySql.Reservations.AnyAsync(r => r.CourtId == createrequest.CourtId /*&& r.ScheduleId == createrequest.ScheduleId*/ && !r.IsDeleted))
            {
                return new OperationResponse<long>(0, false, "Ésta cancha ya está reservada en ese horario.");
            }

            PaymentMethod paymentMethod = await _contextMySql.PaymentMethods.FirstOrDefaultAsync(p => p.Id == createrequest.PaymentMethodId && !p.IsDeleted);

            // Si existe el método de pago ingresado.
            if (paymentMethod == null)
            {
                return new OperationResponse<long>(0, false, "El método de pago ingresado es incorrecto.");
            }
            #endregion

            Reservation newReservation = new()
            {
                Timestamp = DateTime.Now.ToLocalTime(),
                ReservationStatusId = (long)EReservationStatus.Nueva,
                ClientId = createrequest.ClientId,
                CourtId = createrequest.CourtId,
                TurnId = createrequest.TurnId,
                PaymentMethodId = createrequest.PaymentMethodId,
            };

            var manualReservation = await _contextMySql.Reservations.AddAsync(newReservation);
            await _contextMySql.SaveChangesAsync();

            // Email de reserva creada con éxito.
            await _emailService.ReservationNotification(
                clientName: $"{client.FirstName} {client.LastName}",
                clientEmail: client.EmailAddress,
                turn: $"{turn.StartTime} {"|"} {turn.EndTime}",
                court: court.Name,
                reservationStatus: EReservationStatus.Nueva.ToString());

            return new OperationResponse<long>(manualReservation.Entity.Id);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoListManualReservation>>> ListManualReservations(string filter, int pageSize, int page, CancellationToken ct = default)
        {
            var query = _contextMySql
                                .Reservations
                                .Include(s => s.Client)
                                .Include(s => s.Court)
                                .Include(s => s.Turn)
                                .Include(s => s.PaymentMethod)
                                .Include(e => e.ReservationStatus)
                                .AsNoTracking()
                                .Where(r => !r.IsDeleted);

            var count = await query.CountAsync(ct).ConfigureAwait(false);

            var list = (await query.OrderBy(p => p.Id)
                      .Skip(page * pageSize)
                      .Take(pageSize)
                      .ToListAsync(ct)
                      .ConfigureAwait(false))
                      .Select(r => new DtoListManualReservation()
                      {
                          Id = r.Id,
                          Client = $"{r.Client.FirstName} {r.Client.LastName}",
                          Court = r.Court.Name,
                          //Schedule = $"{r.Schedule.StartDate} {r.Schedule.EndDate}",
                          ReservationStatus = r.ReservationStatus.Label,
                          PaymentMethod = r.PaymentMethod.Label,
                          //OperationId = r.OperationId.ToString(),
                          IsRegistered = r.Client.IsRegistered ? "SI" : "NO",
                      });

            return new OperationResponse<DtoResponsePagination<DtoListManualReservation>>(new DtoResponsePagination<DtoListManualReservation>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

        public async Task<OperationResponse<long>> DeleteReserve(long id, CancellationToken ct = default)
        {
            Reservation reserve = await _contextMySql
                                .Reservations
                                .FirstOrDefaultAsync(c => c.Id == id, ct)
                                .ConfigureAwait(false);

            if (reserve == null)
                return new OperationResponse<long>(0, false, "La reserva seleccionada no existe");

            reserve.IsDeleted = true;
            reserve.ReservationStatusId = (long)EReservationStatus.Cancelada;

            _contextMySql.Reservations.Update(reserve);
            await _contextMySql.SaveChangesAsync(ct);

            // Email de reserva cancelada.
            await _emailService.ReservationNotification(
                clientName: $"{reserve.Client.FirstName} {reserve.Client.FirstName}",
                clientEmail: reserve.Client.EmailAddress,
                turn: $"{reserve.Turn.StartTime} {reserve.Turn.EndTime}",
                court: reserve.Court.Name,
                reservationStatus: EReservationStatus.Cancelada.ToString());

            return new OperationResponse<long>(reserve.Id);
        }

    }
}
