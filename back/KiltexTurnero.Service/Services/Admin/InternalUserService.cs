﻿using KiltexTurnero.Domain;
using KiltexTurnero.Domain.Models;
using KiltexTurnero.SDK.Security;
using KiltexTurnero.Service.Dtos.Users;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Admin
{
    public class InternalUserService
    {
        internal readonly SQLContext _contextMySql;
        public InternalUserService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<DtoGetInternalUser>> GetById(long id)
        {
            InternalUser user = await _contextMySql
                                        .InternalUsers
                                        .AsNoTracking()
                                        .FirstOrDefaultAsync(u => u.Id == id && !u.IsDeleted)
                                        .ConfigureAwait(false);

            // Comprobación: si el usuario existe
            if (user == null)
            {
                return new OperationResponse<DtoGetInternalUser>(null, false, "El usuario ingresado no existe");
            }

            var dto = new DtoGetInternalUser()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                EmailAddress = user.EmailAddress,
            };
            return new OperationResponse<DtoGetInternalUser>(dto);
        }

        public async Task<OperationResponse<long>> Post(DtoAddInternalUser createrequest)
        {
            #region Comprobaciones
            // Comprobación: Si existe un usuario con el mismo email.
            if (await _contextMySql.InternalUsers.AnyAsync(c => c.EmailAddress == createrequest.EmailAddress))
            {
                return new OperationResponse<long>(0, false, "Ya existe un usuario con el mismo e-mail");
            }

            // Comprobación: Si existe un cliente con el mismo email.
            if (await _contextMySql.Clients.AnyAsync(c => c.EmailAddress == createrequest.EmailAddress))
            {
                return new OperationResponse<long>(0, false, "Ya existe un cliente con el mismo e-mail");
            }

            // Comprobaciones de Nombre y Apellido (length)
            if (createrequest.FirstName.Length < 2)
            {
                return new OperationResponse<long>(0, false, "El Nombre debe tener como mínimo 2 caracteres");
            }

            if (createrequest.FirstName.Length > 19)
            {
                return new OperationResponse<long>(0, false, "El Nombre debe tener como máximo 20 caracteres");
            }

            if (createrequest.LastName.Length < 2)
            {
                return new OperationResponse<long>(0, false, "El Apellido debe tener como mínimo 2 caracteres");
            }

            if (createrequest.LastName.Length > 19)
            {
                return new OperationResponse<long>(0, false, "El Apellido debe tener como máximo 20 caracteres");
            }

            // Comprobación de formato de E-Mail
            static bool IsValid(string emailaddress)
            {
                try
                {
                    MailAddress m = new(emailaddress);

                    return true;
                }
                catch (FormatException)
                {
                    return false;
                }
            }
            if (!IsValid(createrequest.EmailAddress))
            {
                return new OperationResponse<long>(0, false, "Ingrese un E-mail válido.");
            }

            //Comprobación: Password
            if (createrequest.Password.Length < 8)
            {
                return new OperationResponse<long>(0, false, "La contraseña debe tener como mínimo 8 caracteres");
            }

            if (!(createrequest.Password.Any(c => char.IsDigit(c)) && createrequest.Password.Any(c => char.IsLetter(c))))
            {
                return new OperationResponse<long>(0, false, "La contraseña debe ser alfanumérica");
            }
            #endregion

            InternalUser newUser = new()
            {
                FirstName = createrequest.FirstName,
                LastName = createrequest.LastName,
                EmailAddress = createrequest.EmailAddress,
                Password = SecurePasswordHasher.Hash(createrequest.Password),
            };

            var user = await _contextMySql.InternalUsers.AddAsync(newUser);
            await _contextMySql.SaveChangesAsync();

            return new OperationResponse<long>(user.Entity.Id);
        }

        public async Task<OperationResponse<long>> Update(DtoEditInternalUser updaterequest)
        {
            InternalUser user = await _contextMySql
                                    .InternalUsers
                                    .FirstOrDefaultAsync(u => u.Id == updaterequest.Id && !u.IsDeleted);

            if (user == null) return new OperationResponse<long>(updaterequest.Id, false, "El usuario no está registrado o no existe.");

            #region Comprobaciones
            // Comprobaciones de Nombre y Apellido (length)
            if (updaterequest.FirstName.Length < 2)
            {
                return new OperationResponse<long>(0, false, "El Nombre debe tener como mínimo 2 caracteres");
            }

            if (updaterequest.LastName.Length < 2)
            {
                return new OperationResponse<long>(0, false, "El Apellido debe tener como mínimo 2 caracteres");
            }

            // Comprobación: Password (verificación y caracteres requeridos)
            if (!(string.IsNullOrEmpty(updaterequest.Password)))
            {
                if (SecurePasswordHasher.Verify(updaterequest.Password, user.Password))
                {
                    return new OperationResponse<long>(0, false, "La contraseña nueva debe ser diferente a la contraseña actual");
                }

                if (updaterequest.Password.Length < 8)
                {
                    return new OperationResponse<long>(0, false, "La contraseña debe tener como mínimo 8 caracteres");
                }

                if (!(updaterequest.Password.Any(c => char.IsDigit(c)) && updaterequest.Password.Any(c => char.IsLetter(c))))
                {
                    return new OperationResponse<long>(0, false, "La contraseña debe ser alfanumérica");
                }

                user.Password = SecurePasswordHasher.Hash(updaterequest.Password);
            }
            #endregion

            user.Id = updaterequest.Id;
            user.FirstName = updaterequest.FirstName;
            user.LastName = updaterequest.LastName;

            _contextMySql.InternalUsers.Update(user);
            await _contextMySql.SaveChangesAsync();

            return new OperationResponse<long>(updaterequest.Id);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoListInternalUser>>> ListInternalUsers(string filter, int pageSize, int page, CancellationToken ct = default)
        {
            var query = _contextMySql
                                .InternalUsers
                                .AsNoTracking()
                                .Where(u => !u.IsDeleted && (u.FirstName.ToLower().Contains(filter ?? "") ||
                                                             u.LastName.ToLower().Contains(filter ?? "")));

            var count = await query.CountAsync(ct).ConfigureAwait(false);

            var list = (await query.OrderBy(p => p.FirstName)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .ToListAsync(ct)
                                  .ConfigureAwait(false))
                                  .Select(u => new DtoListInternalUser()
                                  {
                                      Id = u.Id,
                                      FirstName = u.FirstName,
                                      LastName = u.LastName,
                                      EmailAddress = u.EmailAddress,
                                  });

            return new OperationResponse<DtoResponsePagination<DtoListInternalUser>>(new DtoResponsePagination<DtoListInternalUser>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

        public async Task<OperationResponse<bool>> DeleteInternalUser(long id, CancellationToken ct = default)
        {
            bool userExist = await _contextMySql
                                .InternalUsers
                                .AnyAsync(u => u.Id == id && !u.IsDeleted)
                                .ConfigureAwait(false);

            if (!userExist)
                return new OperationResponse<bool>(false, false, ("No se puede eliminar éste usuario, ya que no existe."));


            var u = await _contextMySql.InternalUsers.FirstOrDefaultAsync(u => u.Id == id, ct).ConfigureAwait(false);
            u.IsDeleted = true;

            _contextMySql.InternalUsers.Update(u);
            await _contextMySql.SaveChangesAsync(ct);

            return new OperationResponse<bool>(true);
        }

    }
}
