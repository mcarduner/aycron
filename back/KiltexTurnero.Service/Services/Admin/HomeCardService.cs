﻿using KiltexTurnero.Domain;
using KiltexTurnero.Domain.Models;
using KiltexTurnero.Service.Dtos.HomeCard;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Admin
{
    public class HomeCardService
    {
        internal readonly SQLContext _contextMySql;
        public HomeCardService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<DtoGetHomeCard>> GetById(long id)
        {
            HomeCard homeCard = await _contextMySql
                                            .HomeCards
                                            .AsNoTracking()
                                            .FirstOrDefaultAsync(c => c.Id == id)
                                            .ConfigureAwait(false);

            // Comprobación: si la tarjeta existe
            if (homeCard == null)
            {
                return new OperationResponse<DtoGetHomeCard>(null, false, "La tarjeta no existe");
            }

            var dto = new DtoGetHomeCard()
            {
                Id = homeCard.Id,
                Title = homeCard.Title,
                Description = homeCard.Description,
                IsActive = homeCard.IsActive,
            };
            return new OperationResponse<DtoGetHomeCard>(dto);
        }

        public async Task<OperationResponse<long>> AddHomeCard(DtoAddHomeCard createrequest)
        {
            //// Comprobación: Existencia de sólo 3 tarjetas activas.
            //int count = await _contextMySql.HomeCards.CountAsync(c => c.IsActive && !c.IsDeleted);

            //if (count > 2 && createrequest.IsActive)
            //{
            //    return new OperationResponse<long>(0, false, "Solo pueden haber tres tarjetas activas, desactive una antes de activar ésta");
            //}

            // Comprobación: Si existe esa tarjeta.
            if (await _contextMySql.HomeCards.AnyAsync(c => c.Title == createrequest.Title && !c.IsDeleted))
            {
                return new OperationResponse<long>(0, false, "Ya existe esa tarjeta");
            }

            HomeCard newCard = new()
            {
                Title = createrequest.Title,
                Description = createrequest.Description,
                IsActive = createrequest.IsActive,
            };

            var homeCard = await _contextMySql.HomeCards.AddAsync(newCard);
            await _contextMySql.SaveChangesAsync();

            return new OperationResponse<long>(homeCard.Entity.Id);
        }

        public async Task<OperationResponse<long>> EditHomeCard(DtoEditHomeCard updaterequest)
        {
            //// Comprobación: Existencia de sólo 3 tarjetas activas.
            //int count = await _contextMySql.HomeCards.CountAsync(c => c.IsActive && !c.IsDeleted);

            //if (count > 2 && updaterequest.IsActive)
            //{
            //    return new OperationResponse<long>(0, false, "Solo pueden haber tres tarjetas activas, desactive una antes de activar ésta");
            //}

            // Comprobación: Si existe esa tarjeta.
            if (await _contextMySql.HomeCards.AnyAsync(c => c.Title == updaterequest.Title && !c.IsDeleted && c.Id != updaterequest.Id))
            {
                return new OperationResponse<long>(0, false, "Ya existe esa tarjeta");
            }

            HomeCard homeCard = await _contextMySql
                                    .HomeCards
                                    .FirstOrDefaultAsync(p => p.Id == updaterequest.Id && !p.IsDeleted);

            if (homeCard == null) return new OperationResponse<long>(updaterequest.Id, false);

            homeCard.Id = updaterequest.Id;
            homeCard.Title = updaterequest.Title;
            homeCard.Description = updaterequest.Description;
            homeCard.IsActive = updaterequest.IsActive;

            _contextMySql.HomeCards.Update(homeCard);
            await _contextMySql.SaveChangesAsync();

            return new OperationResponse<long>(updaterequest.Id);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoListHomeCard>>> ListHomeCard(string filter, int pageSize, int page, CancellationToken ct = default)
        {
            var query = _contextMySql
                                  .HomeCards
                                  .AsNoTracking()
                                  .Where(p => !p.IsDeleted && p.Title.ToLower().Contains(filter ?? ""));

            var count = await query.CountAsync(ct).ConfigureAwait(false);

            var list = (await query.OrderBy(p => p.Title)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .ToListAsync(ct)
                                  .ConfigureAwait(false))
                                  .Select(c => new DtoListHomeCard()
                                  {
                                      Id = c.Id,
                                      Title = c.Title,
                                      Description = c.Description,
                                      IsActive = c.IsActive
                                  });

            return new OperationResponse<DtoResponsePagination<DtoListHomeCard>>(new DtoResponsePagination<DtoListHomeCard>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

        public async Task<OperationResponse<long>> Delete(long id, CancellationToken ct = default)
        {
            HomeCard homeCard = await _contextMySql
                                         .HomeCards
                                         .FirstOrDefaultAsync(p => p.Id == id && !p.IsDeleted, ct)
                                         .ConfigureAwait(false);
            if (homeCard != null)
            {
                homeCard.IsDeleted = true;
                await _contextMySql.SaveChangesAsync(ct).ConfigureAwait(false);
            }

            return new OperationResponse<long>(homeCard.Id);
        }

    }

}
