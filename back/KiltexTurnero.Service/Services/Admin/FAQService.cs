﻿using KiltexTurnero.Domain;
using KiltexTurnero.Domain.Models;
using KiltexTurnero.Service.Dtos.FAQ;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Admin
{
    public class FAQService
    {
        internal readonly SQLContext _contextMySql;

        public FAQService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<DtoGetFaq>> GetById(long id)
        {
            FAQ faq = await _contextMySql
                        .FAQs
                        .AsNoTracking()
                        .FirstOrDefaultAsync(c => c.Id == id)
                        .ConfigureAwait(false);

            // Comprobación: si el FAQ existe
            if (faq == null)
            {
                return new OperationResponse<DtoGetFaq>(null, false, "El FAQ ingresado no existe");
            }

            var dto = new DtoGetFaq()
            {
                Id = faq.Id,
                Title = faq.Title,
                Question = faq.Question,
                Answer = faq.Answer,
            };
            return new OperationResponse<DtoGetFaq>(dto);
        }

        public async Task<OperationResponse<long>> Post(DtoAddFaq createrequest)
        {
            // Comprobación: Si existe esa FAQ.
            if (await _contextMySql.FAQs.AnyAsync(c => c.Title == createrequest.Title && !c.IsDeleted))
            {
                return new OperationResponse<long>(0, false, "Ya existe esa pregunta");
            }

            FAQ newfaq = new()
            {
                Title = createrequest.Title,
                Question = createrequest.Question,
                Answer = createrequest.Answer,
            };

            var faq = await _contextMySql.FAQs.AddAsync(newfaq);
            await _contextMySql.SaveChangesAsync();

            return new OperationResponse<long>(faq.Entity.Id);
        }

        public async Task<OperationResponse<long>> Update(DtoEditFaq updaterequest)
        {
            // Comprobación: Si existe esa FAQ.
            if (await _contextMySql.FAQs.AnyAsync(c => c.Title == updaterequest.Title && !c.IsDeleted && c.Id != updaterequest.Id))
            {
                return new OperationResponse<long>(0, false, "Ya existe esa pregunta");
            }

            FAQ faq = await _contextMySql
                                    .FAQs
                                    .FirstOrDefaultAsync(p => p.Id == updaterequest.Id && !p.IsDeleted);

            if (faq == null) return new OperationResponse<long>(updaterequest.Id, false);

            faq.Id = updaterequest.Id;
            faq.Title = updaterequest.Title;
            faq.Question = updaterequest.Question;
            faq.Answer = updaterequest.Answer;

            _contextMySql.FAQs.Update(faq);
            await _contextMySql.SaveChangesAsync();

            return new OperationResponse<long>(updaterequest.Id);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoListFaq>>> ListFAQs(string filter, int pageSize, int page, CancellationToken ct = default)
        {
            var query = _contextMySql
                                  .FAQs
                                  .AsNoTracking()
                                  .Where(p => !p.IsDeleted && p.Title.ToLower().Contains(filter ?? ""));

            var count = await query.CountAsync().ConfigureAwait(false);

            var list = (await query.OrderBy(p => p.Title)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .ToListAsync(ct)
                                  .ConfigureAwait(false))
                                  .Select(c => new DtoListFaq()
                                  {
                                      Id = c.Id,
                                      Title = c.Title,
                                      Question = c.Question,
                                      Answer = c.Answer
                                  });

            return new OperationResponse<DtoResponsePagination<DtoListFaq>>(new DtoResponsePagination<DtoListFaq>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

        public async Task<OperationResponse<long>> Delete(long id, CancellationToken ct = default)
        {
            FAQ faq = await _contextMySql
                                         .FAQs
                                         .FirstOrDefaultAsync(p => p.Id == id && !p.IsDeleted, ct)
                                         .ConfigureAwait(false);
            if (faq != null)
            {
                faq.IsDeleted = true;
                await _contextMySql.SaveChangesAsync(ct).ConfigureAwait(false);
            }

            return new OperationResponse<long>(faq.Id);
        }

    }
}
