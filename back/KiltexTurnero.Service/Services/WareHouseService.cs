﻿using AycronTest.Domain.Models;
using AycronTest.Service.Dtos.WareHouse;
using KiltexTurnero.Domain;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace AycronTest.Service.Services
{
    public class WareHouseService
    {
        private readonly SQLContext _context;

        public WareHouseService(SQLContext sqlContext)
        {
            _context = sqlContext;
        }

        public async Task<OperationResponse<DtoGetWareHouse>> GetById(long id)
        {
            var warehouse = await _context
                        .WareHouses
                        .AsNoTracking()
                        .FirstOrDefaultAsync(c => c.Id == id && c.Status == (short)eStatus.Active)
                        .ConfigureAwait(false);

            var dto = new DtoGetWareHouse()
            {
                Id = warehouse.Id,
                Name = warehouse.Name,
                Address = warehouse.Address,
                Code = warehouse.Code,
                Country = warehouse.Country,
                County = warehouse.County,
                Geo = new DtoGeo { lat = warehouse.Location.InteriorPoint.X, lng = warehouse.Location.InteriorPoint.Y },
                State = warehouse.State,
                Zip = warehouse.Zip

            };
            return new OperationResponse<DtoGetWareHouse>(dto);
        }



        public async Task<OperationResponse<long>> Post(DtoAddWareHouse request)
        {


            try
            {
                #region Comprobaciones                    

                if (await _context.WareHouses.AnyAsync(c => c.Name.ToLower() == request.Name.ToLower() && c.Status == (short)eStatus.Active))
                {
                    return new OperationResponse<long>(0, false, "Exist WareHouse with the same name");
                }
                #endregion

                WareHousesModel warehouse = new()
                {
                    Code = request.Code,
                    Name = request.Name,
                    Address = request.Address,
                    Country = request.Country,
                    County = request.County,
                    Zip = request.Zip,
                    Location = new NetTopologySuite.Geometries.Point(request.Geo.lat, request.Geo.lng) { SRID = 4326 },
                    Status = (short)eStatus.Active,
                    State = request.State
                };


                await _context.WareHouses.AddAsync(warehouse);
                await _context.SaveChangesAsync();
                return new OperationResponse<long>(warehouse.Id);
            }
            catch (Exception ex)
            {

                throw;
            }


        }

        public async Task<OperationResponse<long>> Update(DtoUpdateWareHouse request)
        {
            try
            {
                #region Comprobaciones              

                if (await _context.WareHouses.AnyAsync(c => c.Name == request.Name && c.Status == (short)eStatus.Active && c.Id != request.Id))
                {
                    return new OperationResponse<long>(0, false, "Exist WareHouse with the same name");
                }
                #endregion


                var warehouse = await _context.WareHouses.FirstOrDefaultAsync(c => c.Id == request.Id);
                warehouse.Code = request.Code;
                warehouse.Name = request.Name;
                warehouse.Address = request.Address;
                warehouse.Country = request.Country;
                warehouse.County = request.County;
                warehouse.Zip = request.Zip;
                warehouse.Location = new NetTopologySuite.Geometries.Point(request.Geo.lat, request.Geo.lng) { SRID = 4326 };
                warehouse.State = request.State;



                _context.WareHouses.Update(warehouse);
                await _context.SaveChangesAsync();
                return new OperationResponse<long>(warehouse.Id);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoListWareHouse>>> List(string filter, int pageSize, int page, CancellationToken ct = default)
        {
            var query = _context.WareHouses
                                  .AsNoTracking()
                                  .Where(c => c.Status == (short)eStatus.Active && c.Name.ToLower().Contains(filter ?? ""));

            var count = await query.CountAsync().ConfigureAwait(false);

            var list = (await query.OrderBy(p => p.Name)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .ToListAsync(ct)
                                  .ConfigureAwait(false))
                                  .Select(c => new DtoListWareHouse()
                                  {
                                      Id = c.Id,
                                      Name = c.Name,
                                      Address = c.Address,
                                      Code = c.Code,
                                      Country = c.Country,
                                      County = c.County,
                                      State = c.State,
                                      Zip = c.Zip
                                  });

            return new OperationResponse<DtoResponsePagination<DtoListWareHouse>>(new DtoResponsePagination<DtoListWareHouse>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

        public async Task<OperationResponse<List<DtoListWareHouse>>> ListLocation(DtoLocationWareHouse request, CancellationToken ct = default)
        {
            var point = new NetTopologySuite.Geometries.Point(request.Geo.lat, request.Geo.lng) { SRID = 4326 };

            var warehouses = (await _context.WareHouses
                                  .AsNoTracking()
                                  .Where(c => c.Status == (short)eStatus.Active)
                                  .OrderBy(c => c.Location.Distance(point))
                                  .Take(request.Count)
                                  .ToListAsync())
                                  .Select(p => new DtoListWareHouse
                                  {

                                      County = p.County,
                                      Code = p.Code,
                                      Name = p.Name,
                                      Id = p.Id,
                                      State = p.State,
                                      Address = p.Address,
                                      Distance = Math.Round(p.Location.Distance(point) * 1000.0,2),
                                      Geo = new DtoGeo { lat = p.Location.InteriorPoint.X, lng = p.Location.InteriorPoint.Y }
                                  })
                                  ;


            return new OperationResponse<List<DtoListWareHouse>>(warehouses.ToList());

        }

        public async Task<OperationResponse<long>> Delete(long id, CancellationToken ct = default)
        {
            WareHousesModel werehouse = await _context.WareHouses
                                            .FirstOrDefaultAsync(p => p.Id == id && p.Status == (short)eStatus.Active, ct)
                                            .ConfigureAwait(false);
            if (werehouse != null)
            {
                werehouse.Status = (short)eStatus.Deleted;
                await _context.SaveChangesAsync(ct).ConfigureAwait(false);
            }

            return new OperationResponse<long>(werehouse.Id);
        }

    }
}
