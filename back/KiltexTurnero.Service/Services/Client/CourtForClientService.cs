﻿using KiltexTurnero.Domain;
using KiltexTurnero.Service.Dtos.Court;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Client
{
    public class CourtForClientService
    {
        internal readonly SQLContext _contextMySql;
        public CourtForClientService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<DtoGetCourtForClient>> GetCourtDetail(long id, long? clientID)
        {
            var court = await _contextMySql
                        .Courts
                        .AsNoTracking()
                        .FirstOrDefaultAsync(c => c.Id == id && !c.IsDeleted)
                        .ConfigureAwait(false);

            // Comprobación: si el curso existe y corresponde al student
            if (court == null)
            {
                return new OperationResponse<DtoGetCourtForClient>(null, false, "La cancha no existe.");
            }

            var dto = new DtoGetCourtForClient()
            {
                Name = court.Name,
                Description = court.Description,
                ImageUrl = court.ImageUrl,
                IsEnabled = court.IsEnabled,
            };

            return new OperationResponse<DtoGetCourtForClient>(dto);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoListAllCourtForClient>>> ListMyCourts(string filter, int pageSize, int page, long? clientID = null, CancellationToken ct = default)
        {
            var courtsIds = await _contextMySql
                                    .Reservations
                                    .AsNoTracking()
                                    .Where(r => r.ClientId == clientID && !r.IsDeleted).Select(c => c.CourtId)
                                    .ToListAsync(ct);

            var courts = await _contextMySql
                                    .Courts
                                    .AsNoTracking()
                                    .Where(c => !c.IsDeleted && c.Name.ToLower().Contains(filter ?? ""))
                                    .ToListAsync(ct);

            if (clientID != null)
            {
                courts = courts.Where(c => courtsIds.Contains(c.Id)).ToList();
            }
            else
            {
                courts = courts.Where(c => !courtsIds.Contains(c.Id) && !c.IsDeleted).ToList();
            }

            var count = courts.Count;

            var list = courts.OrderBy(c => c.Name)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .Select(c => new DtoListAllCourtForClient()
                                  {
                                      Name = c.Name,
                                      Description = c.Description,
                                      ImageUrl = c.ImageUrl,
                                      IsEnabled = c.IsEnabled,
                                      Id = c.Id
                                  });

            return new OperationResponse<DtoResponsePagination<DtoListAllCourtForClient>>(new DtoResponsePagination<DtoListAllCourtForClient>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

    }
}