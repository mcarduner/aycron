﻿using KiltexTurnero.Domain;
using KiltexTurnero.Service.Dtos.Schedule;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Threading;
using KiltexTurnero.Domain.Models;
using System.Linq;

namespace KiltexTurnero.Service.Services.Client
{
    public class PublicScheduleService
    {
        internal readonly SQLContext _contextMySql;
        public PublicScheduleService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<DtoGetPublicSchedule>> GetById(long id)
        {
            Schedule turn = await _contextMySql
                                .Schedules
                                .AsNoTracking()
                                .FirstOrDefaultAsync(s => s.Id == id)
                                .ConfigureAwait(false);

            // Comprobación: si el turno existe
            if (turn == null)
            {
                return new OperationResponse<DtoGetPublicSchedule>(null, false, "El turno ingresado no existe");
            }

            var dto = new DtoGetPublicSchedule()
            {
                Id = turn.Id,
                StartDate = turn.StartDate.ToLocalTime(),
                EndDate = turn.EndDate.ToLocalTime(),
            };
            return new OperationResponse<DtoGetPublicSchedule>(dto);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoListPublicSchedule>>> ListPublicSchedules(string filter, int pageSize, int page, CancellationToken ct = default)
        {
            var query = _contextMySql
                                  .Schedules
                                  .AsNoTracking()
                                  .Where(s => !s.IsDeleted && s.StartDate.ToString().ToLower().Contains(filter ?? ""));

            var count = await query.CountAsync().ConfigureAwait(false);

            var list = (await query.OrderBy(s => s.StartDate)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .ToListAsync(ct)
                                  .ConfigureAwait(false))
                                  .Select(s => new DtoListPublicSchedule()
                                  {
                                      Id = s.Id,
                                      StartDate = s.StartDate.ToLocalTime(),
                                      EndDate = s.EndDate.ToLocalTime(),
                                  });

            return new OperationResponse<DtoResponsePagination<DtoListPublicSchedule>>(new DtoResponsePagination<DtoListPublicSchedule>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }
    }
}
