﻿using KiltexTurnero.Domain;
using KiltexTurnero.Service.Dtos.HomeCard;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Client
{
    public class HomeCardForClientService
    {
        internal readonly SQLContext _contextMySql;
        public HomeCardForClientService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<List<DtoListHomeCardForClient>>> ListHomeCardForClient(CancellationToken ct = default)
        {
            return new OperationResponse<List<DtoListHomeCardForClient>>(
                await _contextMySql
                        .HomeCards
                        .AsNoTracking()
                        .Where(c => !c.IsDeleted)
                        .Select(c => new DtoListHomeCardForClient()
                        {
                            Title = c.Title,
                            Description = c.Description,
                        }).ToListAsync(ct)
                        );
        }
    }
}
