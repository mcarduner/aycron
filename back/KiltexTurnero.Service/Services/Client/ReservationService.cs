﻿using KiltexTurnero.Cross.Enums;
using KiltexTurnero.Domain;
using KiltexTurnero.Domain.Models;
using KiltexTurnero.Service.Dtos.Reservation;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using KiltexTurnero.Service.Services.Admin;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Client
{
    public class ReservationService
    {
        internal readonly SQLContext _contextMySql;
        private EmailService _emailService;

        public ReservationService(SQLContext contextMySql, EmailService emailService)
        {
            _contextMySql = contextMySql;
            _emailService = emailService;
        }

        public async Task<OperationResponse<DtoGetReservation>> GetReservationDetail(long id, long? clientId)
        {
            bool hasReserved = clientId != null && _contextMySql.Reservations.Any(r => r.ClientId == clientId && r.Id == id);

            var reservation = await _contextMySql
                                .Reservations
                                .Include(r => r.Court)
                                .Include(r => r.Turn)
                                .Include(r => r.ReservationStatus)
                                .Include(r => r.PaymentMethod)
                                .AsNoTracking()
                                .FirstOrDefaultAsync(r => r.Id == id && !r.IsDeleted)
                                .ConfigureAwait(false);

            // Comprobaciones
            if (reservation == null)
            {
                return new OperationResponse<DtoGetReservation>(null, false, "La reserva no existe.");
            }

            var dto = new DtoGetReservation()
            {
                Court = reservation.Court.Name,
                //Schedule = $"{reservation.Schedule.StartDate:dd-MM-yyyy HH-mm} {reservation.Schedule.EndDate:dd-MM-yyyy HH-mm}",
                ReservationStatus = reservation.ReservationStatus.Label,
                PaymentMethod = reservation.PaymentMethod.Label,
            };

            return new OperationResponse<DtoGetReservation>(dto);
        }

        public async Task<OperationResponse<long>> NewReservation(DtoNewReservation request, long clientId)
        {
            #region Comprobaciones
            // Definir métodos de pago disponibles (MP)
            var acceptedPaymentMethods = new List<long>() { (long)EPaymentMethod.MercadoPago, (long)EPaymentMethod.Efectivo };
            if (!acceptedPaymentMethods.Contains(request.PaymentMethodId))
            {
                return new OperationResponse<long>(0, false, "Ingrese un método de pago válido.");
            }

            Domain.Models.Client client = await _contextMySql.Clients.FirstOrDefaultAsync(u => u.Id == clientId && !u.IsDeleted);

            // Si el Id corresponde al cliente logeado
            if (client == null)
            {
                return new OperationResponse<long>(0, false, "El ID no está asociado a ningún cliente.");
            }

            if (!client.IsRegistered)
            {
                return new OperationResponse<long>(0, false, "Debes registrarte en la app para hacer una reserva.");
            }

            Court court = await _contextMySql.Courts.FirstOrDefaultAsync(c => c.Id == request.CourtId);

            // Si existe la cancha
            if (court == null)
            {
                return new OperationResponse<long>(0, false, "La cancha no existe.");
            }

            Turn turn = await _contextMySql.Turn.FirstOrDefaultAsync(s => s.Id == request.TurnId);

            // Si la cancha está abierta en el horario seleccionado
            //if (court.OpeningTime < schedule.StartDate || court.ClosingTime < schedule.EndDate)
            //{
            //    return new OperationResponse<long>(0, false, "Seleccione un horario disponible para ésta cancha.");
            //}

            // Si el cliente ya tiene una reserva activa en ésa cancha en ese horario
            //if (await _contextMySql.Reservations.AnyAsync(r => r.ClientId == clientId && !r.IsDeleted
            //                                                && r.CourtId == request.CourtId /*&& r.ScheduleId == request.ScheduleId*/))
            //{
            //    return new OperationResponse<long>(0, false, "El cliente ya tiene una reserva en ésa cancha en el horario seleccionado.");
            //}

            PaymentMethod paymentMethod = await _contextMySql.PaymentMethods.FirstOrDefaultAsync(c => c.Id == request.PaymentMethodId && !c.IsDeleted);

            // Si existe el método de pago
            if (paymentMethod == null)
            {
                return new OperationResponse<long>(0, false, "El método de pago no existe. Ingrese un método de pago disponible.");
            }

            #endregion

            Reservation reservation = new()
            {
                ClientId = clientId,
                CourtId = request.CourtId,
                TurnId = request.TurnId,
                PaymentMethodId = request.PaymentMethodId,
                Timestamp = DateTime.Now,
                ReservationStatusId = (long)EReservationStatus.Nueva
            };

            var newReserve = await _contextMySql.Reservations.AddAsync(reservation);
            await _contextMySql.SaveChangesAsync();

            // Email de reserva tomada con éxito.
            await _emailService.ReservationNotification(
                clientName: $"{client.FirstName} {client.LastName}",
                clientEmail: client.EmailAddress,
                turn: $"{turn.StartTime} {"|"} {turn.EndTime}",
                court: court.Name,
                reservationStatus: EReservationStatus.Nueva.ToString());

            return new OperationResponse<long>(newReserve.Entity.Id);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoListMyReservation>>> ListMyReservations(string filter, int pageSize, int page, long? clientId = null, CancellationToken ct = default)
        {
            var reservationsIds = await _contextMySql
                                    .Reservations
                                    .AsNoTracking()
                                    .Where(r => r.ClientId == clientId && !r.IsDeleted).Select(r => r.Id)
                                    .ToListAsync(ct);

            var reservation = await _contextMySql
                                    .Reservations
                                    .AsNoTracking()
                                    .Include(r => r.Turn)
                                    .Include(r => r.Court)
                                    .Include(r => r.ReservationStatus)
                                    .Where(r => !r.IsDeleted && r.Court.Name.ToLower().Contains(filter ?? ""))
                                    .ToListAsync(ct);

            if (clientId != null)
            {
                reservation = reservation.Where(r => reservationsIds.Contains(r.Id)).ToList();
            }
            else
            {
                reservation = reservation.Where(r => !reservationsIds.Contains(r.Id) && !r.IsDeleted).ToList();
            }

            var count = reservation.Count;

            var list = reservation.OrderBy(r => r.Court.Name)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .Select(r => new DtoListMyReservation()
                                  {
                                      Id = r.Id,
                                      Court = r.Court.Name,
                                      Turn = $"{r.Turn.StartTime:dd-MM-yyyy HH:mm} {r.Turn.EndTime:dd-MM-yyyy HH:mm}",
                                      ReservationStatus = r.ReservationStatus.Label
                                  });

            return new OperationResponse<DtoResponsePagination<DtoListMyReservation>>(new DtoResponsePagination<DtoListMyReservation>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

    }
}
