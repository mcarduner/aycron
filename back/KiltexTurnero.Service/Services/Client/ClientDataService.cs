﻿using KiltexTurnero.Domain;
using KiltexTurnero.SDK.Security;
using KiltexTurnero.Service.Dtos.Client;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Client
{
    public class ClientDataService
    {
        internal readonly SQLContext _contextMySql;
        public ClientDataService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<DtoGetClientData>> GetClientData(long id)
        {
            var student = await _contextMySql.Clients.FirstOrDefaultAsync(s => s.Id == id && !s.IsDeleted);

            if (student == null) return new OperationResponse<DtoGetClientData>(null, false, "No se encuentra el usuario");

            var dto = new DtoGetClientData()
            {
                FirstName = student.FirstName,
                LastName = student.LastName,
                EmailAddress = student.EmailAddress,
            };

            return new OperationResponse<DtoGetClientData>(dto);
        }

        public async Task<OperationResponse<long>> UpdateClientData(long clientID, DtoUpdateClientData updaterequest, CancellationToken ct = default)
        {
            var client = await _contextMySql
                                    .Clients
                                    .FirstOrDefaultAsync(s => s.Id == clientID && !s.IsDeleted, ct);

            if (client == null) return new OperationResponse<long>(clientID, false, "No se encuentra el usuario");

            #region Comprobaciones
            // Comprobaciones de Nombre y Apellido (length)
            if (updaterequest.FirstName.Length < 2)
            {
                return new OperationResponse<long>(0, false, "El Nombre debe tener como mínimo 2 caracteres");
            }

            if (updaterequest.LastName.Length < 2)
            {
                return new OperationResponse<long>(0, false, "El Apellido debe tener como mínimo 2 caracteres");
            }

            // Comprobación: Cambiar Password (verificación y caracteres requeridos)
            if (!(string.IsNullOrEmpty(updaterequest.Password)))
            {
                if (SecurePasswordHasher.Verify(updaterequest.Password, client.Password))
                {
                    return new OperationResponse<long>(0, false, "La contraseña nueva debe ser diferente a la contraseña actual");
                }

                if (updaterequest.Password.Length < 8)
                {
                    return new OperationResponse<long>(0, false, "La contraseña debe tener como mínimo 8 caracteres");
                }

                if (!(updaterequest.Password.Any(c => char.IsDigit(c)) && updaterequest.Password.Any(c => char.IsLetter(c))))
                {
                    return new OperationResponse<long>(0, false, "La contraseña debe ser alfanumérica");
                }

                client.Password = SecurePasswordHasher.Hash(updaterequest.Password);
            }
            #endregion

            client.FirstName = updaterequest.FirstName;
            client.LastName = updaterequest.LastName;

            _contextMySql.Clients.Update(client);
            await _contextMySql.SaveChangesAsync(ct);

            return new OperationResponse<long>(clientID);
        }

    }
}
