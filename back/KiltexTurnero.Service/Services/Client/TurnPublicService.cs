﻿using KiltexTurnero.Service.Dtos.Turn;
using System.Threading.Tasks;
using KiltexTurnero.Domain;
using KiltexTurnero.Service.OperationResponse;
using KiltexTurnero.Domain.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using KiltexTurnero.Service.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;

namespace KiltexTurnero.Service.Services.Client
{
    public class TurnPublicService
    {
        internal readonly SQLContext sqlContext;

        public TurnPublicService( SQLContext sqlContext)
        {
            this.sqlContext = sqlContext; 
        }

       
        public async Task<OperationResponse<long>> Post( TurnAddDto createrequest)
        {
            Turn newPublicTurn = new()
            {
                Id = createrequest.Id,
                StartTime = createrequest.StartTime,
                EndTime = createrequest.EndTime,
                State = createrequest.State,
                Disabled = createrequest.Disabled,
                ScheduleId = createrequest.ScheduleId,
            };

            var publicTurn = await sqlContext.Turn.AddAsync(newPublicTurn);
            await sqlContext.SaveChangesAsync();

            return new OperationResponse<long>(publicTurn.Entity.Id);
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoGetAvailableTurns>>> GetAvailableTurnsClient(int pageSize, int page, DateTime fecha, long idCourt, CancellationToken ct = default)
        {
            var query = sqlContext.Turn
                        .AsNoTracking()
                        .Where(t => t.State && !t.IsDeleted && (t.StartTime.Date == fecha) && t.Schedule.IdCourt == idCourt);

            var count = await query.CountAsync().ConfigureAwait(false);

            var list = (await query
                        .OrderBy(p => p.Id)
                        .Skip(page * pageSize)
                        .Take(pageSize)
                        .ToListAsync(ct)
                        .ConfigureAwait(false))
                        .Select(t => new DtoGetAvailableTurns()
                        {
                            Id = t.Id,
                            StartTime = t.StartTime.ToString(),
                            EndTime = t.EndTime.ToString(),
                        });

            return new OperationResponse<DtoResponsePagination<DtoGetAvailableTurns>>(new DtoResponsePagination<DtoGetAvailableTurns>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }

        public async Task<OperationResponse<long>> Update(DtoEditTurn request)
        {

            //if (await sqlContext.Turn.AnyAsync(c => c.State == true && !c.IsDeleted))
            //{
            //    return new OperationResponse<long>(0, false, "El turno ya esta disponible");
            //}

            var query = await sqlContext.Turn
                        .FirstOrDefaultAsync(c => c.Id == request.Id && !c.IsDeleted);

            query.Id = request.Id;
            query.State = request.State;

            sqlContext.Update(query);
            await sqlContext.SaveChangesAsync();

            return new OperationResponse<long>(request.Id);
        }
    }
}
