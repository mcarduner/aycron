﻿using KiltexTurnero.Domain;
using KiltexTurnero.Service.Dtos.FAQ;
using KiltexTurnero.Service.Models;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Client
{
    public class FAQClientService
    {
        internal readonly SQLContext _contextMySql;
        public FAQClientService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<DtoResponsePagination<DtoListFaqClient>>> ListFAQs(string filter, int pageSize, int page, CancellationToken ct = default)
        {
            var query = _contextMySql
                                  .FAQs
                                  .AsNoTracking()
                                  .Where(p => !p.IsDeleted && p.Title.ToLower().Contains(filter ?? "") || p.Question.ToLower().Contains(filter ?? "") || p.Answer.ToLower().Contains(filter ?? ""));

            var count = await query.CountAsync().ConfigureAwait(false);

            var list = (await query.OrderBy(p => p.Title)
                                  .Skip(page * pageSize)
                                  .Take(pageSize)
                                  .ToListAsync(ct)
                                  .ConfigureAwait(false))
                                  .Select(c => new DtoListFaqClient()
                                  {
                                      Id = c.Id,
                                      Title = c.Title,
                                      Question = c.Question,
                                      Answer = c.Answer
                                  });

            return new OperationResponse<DtoResponsePagination<DtoListFaqClient>>(new DtoResponsePagination<DtoListFaqClient>()
            {
                Data = list,
                PageSize = pageSize,
                TotalCount = count
            });
        }
    }
}
