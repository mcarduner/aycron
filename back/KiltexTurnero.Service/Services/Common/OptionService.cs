﻿using KiltexTurnero.Domain;
using KiltexTurnero.Service.Dtos.Option;
using KiltexTurnero.Service.OperationResponse;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KiltexTurnero.Service.Services.Common
{
    public class OptionService
    {
        internal readonly SQLContext _contextMySql;

        public OptionService(SQLContext contextMySql)
        {
            _contextMySql = contextMySql;
        }

        public async Task<OperationResponse<List<DtoOption>>> ListClients(CancellationToken ct = default)
        {
            return new OperationResponse<List<DtoOption>>(
                await _contextMySql
                        .Clients
                        .AsNoTracking()
                        .Where(r => !r.IsDeleted)
                        .Select(r => new DtoOption()
                        {
                            Id = r.Id,
                            Label = $"{r.FirstName} {r.LastName} ({r.EmailAddress})",
                            Code = $"{r.FirstName} {r.LastName} ({r.EmailAddress})",
                        }).ToListAsync(cancellationToken: ct)
                        );
        }

        public async Task<OperationResponse<List<DtoOption>>> ListPaymentMethods(CancellationToken ct = default)
        {
            return new OperationResponse<List<DtoOption>>(
                await _contextMySql
                        .PaymentMethods
                        .AsNoTracking()
                        .Where(r => !r.IsDeleted)
                        .Select(r => new DtoOption()
                        {
                            Id = r.Id,
                            Label = r.Label,
                            Code = r.Code,
                        }).ToListAsync(cancellationToken: ct)
                        );
        }

        public async Task<OperationResponse<List<DtoOption>>> ListCourts(CancellationToken ct = default)
        {
            return new OperationResponse<List<DtoOption>>(
                await _contextMySql
                        .Courts
                        .AsNoTracking()
                        .Where(c => !c.IsDeleted)
                        .Select(c => new DtoOption()
                        {
                            Id = c.Id,
                            Label = c.Name,
                            Code = c.Name,
                        }).ToListAsync(cancellationToken: ct)
                        );
        }

        public async Task<OperationResponse<List<DtoOption>>> ListSchedules(CancellationToken ct = default)
        {
            return new OperationResponse<List<DtoOption>>(
                await _contextMySql
                        .Schedules
                        .AsNoTracking()
                        .Where(c => !c.IsDeleted)
                        .Select(c => new DtoOption()
                        {
                            Id = c.Id,
                            Label = c.StartDate.ToString("dd-MM-yyyy HH-mm"),
                            Code = c.EndDate.ToString("dd-MM-yyyy HH-mm"),
                        }).ToListAsync(cancellationToken: ct)
                        );
        }

        public async Task<OperationResponse<List<DtoOption>>> ListClientReservations(long clientId, CancellationToken ct = default)
        {
            return new OperationResponse<List<DtoOption>>(
                await _contextMySql
                        .Reservations
                        .AsNoTracking()
                        .Where(r => !r.IsDeleted && r.ClientId == clientId)
                        .Select(r => new DtoOption()
                        {
                            Id = r.Id,
                            Label = r.Court.Name,
                            Code = r.Court.Name,
                        }).ToListAsync(cancellationToken: ct)
                        );
        }
    }
}
