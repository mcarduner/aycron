﻿using System.Collections.Generic;

namespace KiltexTurnero.Service.Models
{
    public class DtoResponsePagination<T>
    {
        public IEnumerable<T> Data { get; set; }
        public long TotalCount { get; set; }
        public int PageSize { get; set; }
    }
}
