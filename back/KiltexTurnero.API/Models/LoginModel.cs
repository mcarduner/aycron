﻿using System.ComponentModel.DataAnnotations;

namespace KiltexTurnero.API.Models
{
    public class LoginModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

    }
}
