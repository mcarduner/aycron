﻿using System.ComponentModel.DataAnnotations;

namespace KiltexTurnero.API.Models
{
    public class ResetPasswordModel
    {
        [Required]
        public string Token { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
