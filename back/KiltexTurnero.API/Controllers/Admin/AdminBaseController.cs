﻿using KiltexTurnero.API.Filters;
using KiltexTurnero.Cross.Enums;
using Microsoft.AspNetCore.Mvc;

namespace KiltexTurnero.API.Controllers.Admin
{
    [Route("api/v1/admin/[controller]")]
    //[AllowAccess(Rols = new string[] { CERoles.Manager })]
    public class AdminBaseController : ApiBaseController
    {
    }
}
