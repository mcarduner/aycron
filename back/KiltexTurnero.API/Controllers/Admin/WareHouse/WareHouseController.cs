﻿using AycronTest.Service.Dtos.WareHouse;
using AycronTest.Service.Services;
using ClosedXML.Excel;
using KiltexTurnero.API.Controllers.Admin;
using KiltexTurnero.API.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AycronTest.API.Controllers.Admin.WareHouse
{
    public class WareHouseController : AdminBaseController    
    {
        private readonly WareHouseService _service;
        public WareHouseController(WareHouseService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            return Return(await _service.GetById(id).ConfigureAwait(false));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] DtoAddWareHouse request)
        {
            return Return(await _service.Post(request).ConfigureAwait(false));
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] DtoUpdateWareHouse request)
        {
            return Return(await _service.Update(request).ConfigureAwait(false));
        }

        [HttpPost]
        [Route("list")]
        public async Task<IActionResult> ListByFilter(RequestPaginatedData request)
        {
            return Return(await _service.List(request.Filter, request.PageSize, request.Page).ConfigureAwait(false));
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            return Return(await _service.Delete(id).ConfigureAwait(false));
        }

        [HttpPost]
        [Route("ListLocation")]
        public async Task<IActionResult> ListLocation(DtoLocationWareHouse request)
        {
            return Return(await _service.ListLocation(request).ConfigureAwait(false));
        }


        [HttpPost]
        [Route("listExcel")]
        public async Task<IActionResult> ListExcelByFilter(RequestPaginatedData request)
        {
            var resul = await _service.List(request.Filter, request.PageSize, request.Page).ConfigureAwait(false);

            using var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add("Warehouse");


            #region cabecera
            worksheet.Cell("A1").Value = "Code";
            worksheet.Cell("B1").Value = "Name";
            worksheet.Cell("C1").Value = "State";
            worksheet.Cell("D1").Value = "Country";
            worksheet.Cell("E1").Value = "Zip";
            worksheet.Cell("F1").Value = "Address";


            IXLRange range = worksheet.Range(worksheet.Cell(1, 1), worksheet.Cell(1, 6));
            range.Style.Border.RightBorder = XLBorderStyleValues.Thin;
            range.Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            #endregion

            #region detalle
            var i = 2;
            foreach (var item in resul.Data.Data)
            {
                worksheet.Cell("A" + i).Value = item.Code;
                worksheet.Cell("B" + i).Value = item.Name;
                worksheet.Cell("C" + i).Value = item.State;
                worksheet.Cell("D" + i).Value = item.Country;
                worksheet.Cell("E" + i).Value = item.Zip;
                worksheet.Cell("F" + i).Value = item.Address;
                i += 1;
            }
            #endregion


            var workbookBytes = new byte[0];
            using (var ms = new MemoryStream())
            {
                workbook.SaveAs(ms);
                workbookBytes = ms.ToArray();
            }

            return File(workbookBytes, "application/force-download", "WareHouses.xlsx");
        }
    }
}
