﻿using KiltexTurnero.API.Models;
using KiltexTurnero.Cross.Enums;
using KiltexTurnero.SDK.Security;
using KiltexTurnero.Service.Services.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;

namespace KiltexTurnero.API.Controllers.Auth
{
    [Route("api/v1/Auth")]
    [AllowAnonymous]
    public class AuthController : ApiBaseController
    {
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Login(
            [FromBody] LoginModel model,
            [FromServices] AuthService service,
            [FromServices] IConfiguration config)
        {

            var user = await service.GetInternalUserLogin(model.UserName, model.Password);

            if (!user.Success)
            {
                return Forbid();
            }

            var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, $"{user.Data.Name} {user.Data.LastName}"),
                    new Claim(ClaimTypes.Role, (user.Data.IdRol == 1)? CERoles.Manager : CERoles.Operator ),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim("id", $"{user.Data.Id}")
                };

            var token = JWTService.CreateDefaultToken(
            config["Jwt:Issuer"],
            config["Jwt:Audience"],
               120,
            config["Jwt:SecretKey"],
            authClaims);

            return Ok(new
            {
                user = new
                {
                    email = user.Data.Email,
                    fullName = $"{user.Data.Name} {user.Data.LastName}",
                    rol = CERoles.Manager,
                    expiration = token.ValidTo
                },
                token = new JwtSecurityTokenHandler().WriteToken(token)
            });
        }

        
    }
}
