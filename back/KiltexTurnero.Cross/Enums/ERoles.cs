﻿namespace KiltexTurnero.Cross.Enums
{
    public enum ERoles
    {
        Manager = 1,
        Operator = 2
    }

    public class CERoles
    {
        public const string Manager = "Manager";
        public const string Operator = "Operator";
    }
}
