﻿using AycronTest.Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KiltexTurnero.Domain.Models
{
 
    [Table("Users")]
    public class UserModel : BaseModel
    {

        
        [Column("Name")]
        [MaxLength(50)]
        public string Name { get; set; }

        
        [Column("LastName")]
        [MaxLength(20)]
        public string LastName { get; set; }

        
        [Column("Username")]
        public string Username { get; set; }

        
        [Column("Password")]
        public string Password { get; set; }

       
        [Column("Email")]
        public string Email { get; set; }


        [Column("IdRol")]
        public int IdRol { get; set; }

        [ForeignKey("IdRol")]
        public RolModel Rol { get; set; }


    }
}
