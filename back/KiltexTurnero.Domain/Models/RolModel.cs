﻿using KiltexTurnero.Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AycronTest.Domain.Models
{
    [Table("Roles")]
    public class RolModel : BaseModel
    {

        [Column("Name")]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}
