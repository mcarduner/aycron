﻿using KiltexTurnero.Domain.Models;
using NetTopologySuite.Geometries;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace AycronTest.Domain.Models
{
   

    [Table("WareHouses")]
    public class WareHousesModel : BaseModel
    {

        [Required]
        [Column("Code")]
        [MaxLength(50)]
        public string Code { get; set; }

        [Required]
        [Column("Name")]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [Column("Address")]
        [MaxLength(500)]
        public string Address { get; set; }


        [Column("Country")]
        [MaxLength(100)]
        public string Country { get; set; }


        [Column("County")]
        [MaxLength(100)]
        public string County { get; set; }

        [Column("Zip")]
        [MaxLength(50)]
        public string Zip { get; set; }

        [Required]
        [Column("Location")]
        public Geometry Location { get; set; }

        [Required]
        [Column("State")]
        [MaxLength(100)]
        public string State { get; set; }

        [Required]
        [Column("Status")]
        public short Status { get; set; }
    }

    public enum eStatus
    {
        Deleted = 2,
        Active = 1
    }
}
