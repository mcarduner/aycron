﻿using AycronTest.Domain.Models;
using KiltexTurnero.Cross.Enums;
using KiltexTurnero.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace KiltexTurnero.Domain
{
    public class SQLContext : DbContext
    {
        public SQLContext() : base()
        {
        }

        public SQLContext(DbContextOptions<SQLContext> options)
            : base(options)
        {
        }

        // DB TABLES
        public virtual DbSet<UserModel> Users { get; set; }
        public virtual DbSet<RolModel> Roles { get; set; }
        public virtual DbSet<WareHousesModel> WareHouses { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            AddInitialDataSets(modelBuilder);
        }

        private static void AddInitialDataSets(ModelBuilder modelBuilder)
        {
            
        }

      

        
    }
}
