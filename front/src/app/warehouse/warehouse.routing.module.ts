import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../common/auth/auth.guardian';
import { LayoutComponent } from '../layout/layout.component';
import { WarehouseListComponent } from './components/warehouse-list.component';
import { WarehouseComponent } from './components/warehouse.component';

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home/list',
  },
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'list',
        component: WarehouseListComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WarehouseRoutingModule {}
