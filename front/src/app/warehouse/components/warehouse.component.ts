import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { BaseComponent } from 'src/app/common/components/base.component';
import { MapComponent } from 'src/app/maps/components/map.component';
import { environment } from 'src/environments/environment';
import { WarehouseAddModel } from '../models/warehouse.list.mode';
import { WarehouseService } from '../warehouse.service';

@Component({
  selector: 'wh-warehouse',
  templateUrl: './warehouse.component.html',
})
export class WarehouseComponent extends BaseComponent implements OnInit {
  @Input() id: number | string = '';
  /**
   * Map component
   */
  @ViewChild('mapComponent') map!: MapComponent;

  /**
   * Formulario
   */
  form!: FormGroup;
  /**
   * list of states
   */
  statesList: any[] = [];

  /**
   * list of county
   */
  countyList: any[] = [];

  /**
   * current value
   */
  currentLatLng: google.maps.LatLng | null = null;

  /**
   *
   */
  updateMap: boolean = false;

  isload: boolean = false;

  isSaving: boolean = false;

  /**
   * ctro
   */
  constructor(
    el: ElementRef,
    router: Router,
    messages: NzMessageService,
    private service: WarehouseService,
    private fb: FormBuilder,
    private drawerRef: NzDrawerRef<any>
  ) {
    super(el, messages, router);
    this.intForm();
    this.loadStates();
  }

  /**
   * load the states
   */
  loadStates() {
    this.service.getStates().subscribe((r) => {
      this.statesList = r;
    });
  }

  /**
   * event of state changing
   * @param state
   */
  stateChange(state: any) {
    this.service.getCounty(state).subscribe((r) => {
      this.countyList = r;
      if (!this.isload) {
        this.form.controls['county'].setValue('');
        this.setMarker();
      } else {
        this.isload = false;
        this.loadMarker();
      }
    });
  }

  countyChanged() {
    this.setMarker();
  }

  /**
   * Init event angular
   */
  ngOnInit() {
    this.loadWarehouse();
  }

  /**
   * create the form
   */
  intForm(): void {
    this.form = this.fb.group({
      id: [''],
      geo: [''],
      country: [environment.country],
      code: ['', [Validators.required, Validators.maxLength(10)]],
      name: ['', [Validators.required, Validators.maxLength(200)]],
      state: ['', [Validators.required, Validators.maxLength(200)]],
      county: ['', [Validators.maxLength(200)]],
      zip: ['', [Validators.maxLength(50)]],
      address: ['', [Validators.required, Validators.maxLength(500)]],
    });
  }

  loadMarker() {
    let geo = this.form.controls['geo'].value;
    this.map.setMarker(
      new google.maps.LatLng(geo.lat, geo.lng),
      this.form.controls['name'].value,
      ''
    );
  }
  setMarker() {
    if (this.isload) return;

    let obj = this.form.getRawValue();
    if (obj.address && obj.address.trim() !== '') {
      let county = obj.county;
      let state = obj.state;
      let direction = `${this.form.controls['address'].value},${county || ''},${
        state || ''
      },${environment.country || ''}`;

      this.map.setAddress(direction, obj.name || '', '');
    } else {
      this.map.deleteMarkers();
    }
  }

  loadWarehouse() {
    if (this.id) {
      this.service.getById(this.id).subscribe((p) => {
        this.isload = true;
        this.form.setValue(p);
      });
    }
  }

  /**
   * save the warehouse
   * @returns
   */
  save() {
    if (!this.isValidForm(this.form)) return;

    if (!this.currentLatLng || this.updateMap) {
      this.showMessageError('Please check the address');
      return;
    }

    let obj = this.form.getRawValue();
    obj.geo = this.currentLatLng.toJSON();
    this.isSaving = true;
    this.service
      .save(obj)
      .subscribe((r) => {
        this.showMessageSuccess('The warehouse was saved successfully');
        this.drawerRef.close(obj);
      })
      .add(() => (this.isSaving = false));
  }

  onChangeMarkert(value: google.maps.LatLng | null) {
    this.currentLatLng = value;
    if (value) {
      this.updateMap = false;
    }
  }

  close() {
    this.drawerRef.close();
  }
}
