import { Component, ElementRef, OnInit } from '@angular/core';
import { WarehouseListModel } from '../models/warehouse.list.mode';
import { WarehouseService } from '../warehouse.service';
import { NzDrawerRef, NzDrawerService } from 'ng-zorro-antd/drawer';
import { WarehouseComponent } from './warehouse.component';
import { WarehouseNearestComponent } from './warehouse-nearest.component';
import { BaseComponent } from 'src/app/common/components/base.component';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AuthService } from 'src/app/common/auth/auth.service';

@Component({
  selector: 'wh-list',
  templateUrl: 'warehouse-list.component.html',
})
export class WarehouseListComponent extends BaseComponent implements OnInit {
  listOfColumns: any[] = [];
  listOfWarehouse: WarehouseListModel[] = [];
  listOfWarehouseFiltered: WarehouseListModel[] = [];
  search: string = '';
  constructor(
    el: ElementRef,
    router: Router,
    messages: NzMessageService,
    private service: WarehouseService,
    private drawerService: NzDrawerService,
    private authService: AuthService
  ) {
    super(el, messages, router);
    this.initColumns();
  }

  ngOnInit() {
    this.loadWarehouses();
  }

  initColumns() {
    this.listOfColumns = [
      {
        name: 'Code',
        sortOrder: null,
        sortFn: (a: WarehouseListModel, b: WarehouseListModel) =>
          a.code.localeCompare(b.code),
        sortDirections: ['ascend', 'descend', null],
      },
      {
        name: 'Name',
        sortOrder: null,
        sortFn: (a: WarehouseListModel, b: WarehouseListModel) =>
          a.name.localeCompare(b.name),
        sortDirections: ['ascend', 'descend', null],
      },
      {
        name: 'State',
        sortOrder: null,
        sortFn: (a: WarehouseListModel, b: WarehouseListModel) =>
          a.state.localeCompare(b.state),
        sortDirections: ['ascend', 'descend', null],
      },
      {
        name: 'County',
        sortOrder: null,
        sortFn: (a: WarehouseListModel, b: WarehouseListModel) =>
          (a.county || '').localeCompare(b.county || ''),
        sortDirections: ['ascend', 'descend', null],
      },
      {
        name: 'Zip',
        sortOrder: null,
        sortFn: (a: WarehouseListModel, b: WarehouseListModel) =>
          (a.zip || '').localeCompare(b.zip || ''),
        sortDirections: ['ascend', 'descend', null],
      },
      {
        name: 'Address',
        sortOrder: null,
        sortFn: (a: WarehouseListModel, b: WarehouseListModel) =>
          a.address.localeCompare(b.address),
        sortDirections: ['ascend', 'descend', null],
      },
    ];
  }

  loadWarehouses() {
    this.service.getAllWarehouse().subscribe((p) => {
      this.listOfWarehouse = p.data;
      this.listOfWarehouseFiltered = p.data;
      this.filter();
    });
  }

  filter() {
    let search = this.search.toLocaleLowerCase().trim();
    this.listOfWarehouseFiltered = this.listOfWarehouse.filter(
      (p) =>
        p.code.toLocaleLowerCase().includes(search) ||
        p.name.toLocaleLowerCase().includes(search) ||
        p.state.toLocaleLowerCase().includes(search) ||
        p.county?.toLocaleLowerCase().includes(search) ||
        p.zip?.toLocaleLowerCase().includes(search) ||
        p.address.toLocaleLowerCase().includes(search)
    );
  }

  showDrawer(id?: number | string) {
    const drawerRef = this.drawerService.create<WarehouseComponent, {}, string>(
      {
        nzContent: WarehouseComponent,
        nzWidth: '90%',
        nzContentParams: {
          id: id,
        },
      }
    );

    drawerRef.afterClose.subscribe((data) => {
      if (data) {
        this.loadWarehouses();
      }
    });
  }

  showNearest() {
    const drawerRef = this.drawerService.create<
      WarehouseNearestComponent,
      {},
      string
    >({
      nzContent: WarehouseNearestComponent,
      nzWidth: '90%',
    });

    drawerRef.afterClose.subscribe((data) => {});
  }

  deleteConfirm(id: number | string): void {
    this.service.delete(id).subscribe((p) => {
      this.showMessageSuccess('The record was deleted successfully.');
      this.loadWarehouses();
    });
  }

  get isManager() {
    return this.authService.currentUser.rol === 'Manager';
  }

  export() {
    this.excelIcon = 'loading';
    this.service
      .export(this.search)
      .subscribe()
      .add(() => (this.excelIcon = 'file-excel'));
  }

  excelIcon: string = 'file-excel';
}
