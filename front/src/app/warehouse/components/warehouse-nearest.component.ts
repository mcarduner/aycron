import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MapComponent } from 'src/app/maps/components/map.component';
import { MapService } from 'src/app/maps/map.service';
import { WarehouseNearestModel } from '../models/warehouse.list.mode';
import { WarehouseService } from '../warehouse.service';

@Component({
  selector: 'wh-nearest',
  templateUrl: './warehouse-nearest.component.html',
})
export class WarehouseNearestComponent implements OnInit {
  /**
   * Map component
   */
  @ViewChild('mapComponent') map!: MapComponent;

  @ViewChild('panel') panel!: HTMLElement;
  listOfWarehouse: WarehouseNearestModel[] = [];
  address: string = '';
  count: number = 3;
  isLoading: boolean = false;
  showSpinner: boolean = false;
  /**
   *
   * @param service
   * @param mapService
   */
  constructor(
    private service: WarehouseService,
    private mapService: MapService
  ) {}

  /**
   *
   */
  ngOnInit() {}

  /**
   *
   */
  draw() {
    this.isLoading = true;
    this.mapService
      .getCodeAddress(this.address)
      .subscribe((r: google.maps.GeocoderResult[] | null) => {
        if (r && r.length > 0) {
          var p = r[0];
          this.loadWarehouse(this.count, p.geometry.location, this.address);
        }
      })
      .add(() => (this.isLoading = false));
  }

  /**
   *
   * @param count
   * @param location
   * @param title
   */
  loadWarehouse(count: number, location: google.maps.LatLng, title: string) {
    this.showSpinner = true;
    this.map.deletePolyline();
    this.map.deleteMarkers();
    this.map.deleteDirections();
    document.getElementById('panel')!.innerHTML = '';

    this.service
      .getNearestWarehouse(count, location)
      .subscribe((data: WarehouseNearestModel[]) => {
        this.listOfWarehouse = data;
        if (data.length == 0) {
          return;
        }

        setTimeout(() => {
          this.markNearestWarehouse(location, data[0]);
          this.markWarehouse(location, data);
          this.drawDirection(location, data);
        }, 10);
      })
      .add(() => (this.showSpinner = false));
  }

  /**
   *
   * @param location
   * @param data
   */
  drawDirection(location: google.maps.LatLng, data: WarehouseNearestModel[]) {
    var request = {
      origin: location,
      destination: { lat: data[0].geo.lat, lng: data[0].geo.lng },
      travelMode: google.maps.TravelMode.DRIVING,
    };

    this.map.setDirections(request, document.getElementById('panel'));
  }

  /**
   *
   * @param location
   * @param data
   */
  markWarehouse(location: google.maps.LatLng, data: WarehouseNearestModel[]) {
    let polylineData: any[] = [];

    for (let index = 1; index < data.length; index++) {
      polylineData.push({ lat: location.lat(), lng: location.lng() });
      polylineData.push({ lat: data[index].geo.lat, lng: data[index].geo.lng });
    }

    if (polylineData.length > 0) {
      this.map.setPolyline(polylineData);
    }

    this.map.setMultiplesMarker(
      data.map<any>((p) => {
        return {
          latLng: new google.maps.LatLng(p.geo.lat, p.geo.lng),
          title: p.code,
          content: '<div>' + p.code + '</div>',
          icon: p.icon || '/assets/warehouse.png',
        };
      })
    );
  }

  /**
   *
   * @param location
   * @param warehouse
   */
  markNearestWarehouse(
    location: google.maps.LatLng,
    warehouse: WarehouseNearestModel
  ) {
    this.map.setPolyline(
      [
        { lat: location.lat(), lng: location.lng() },
        { lat: warehouse.geo.lat, lng: warehouse.geo.lng },
      ],
      '#008000'
    );

    this.map.setMultiplesMarker(
      [
        {
          geo: {
            lat: location.lat(),
            lng: location.lng(),
          },
          code: this.address,
          icon: '/assets/mark.png',
        },
        warehouse,
      ].map<any>((p) => {
        return {
          latLng: new google.maps.LatLng(p.geo.lat, p.geo.lng),
          title: p.code,
          content: '<div>' + p.code + '</div>',
          icon: p.icon || '/assets/warehouse.png',
        };
      })
    );
  }
}
