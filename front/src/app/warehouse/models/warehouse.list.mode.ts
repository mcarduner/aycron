export class WarehouseListModel {
  id!: number;
  name!: string;
  code!: string;
  state!: string;
  county?: string;
  address!: string;
  zip?: string;
}

export class WarehouseAddModel {
  id?: number;
  name!: string;
  code!: string;
  state!: string;
  county?: string;
  country?: string;
  address!: string;
  zip?: string;
  geo!: {
    lat: number;
    lng: number;
  };
}

export class WarehouseNearestModel {
  name?: string;
  code!: string;
  address?: string;
  geo!: {
    lat: number;
    lng: number;
  };
  distance?: number;
  icon?: string;
}
