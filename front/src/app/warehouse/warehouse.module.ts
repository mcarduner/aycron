import { NgModule } from '@angular/core';
import { MapsModule } from '../maps/maps.module';
import { WarehouseComponent } from './components/warehouse.component';
import { WarehouseRoutingModule } from './warehouse.routing.module';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { CommonModule } from '../common/common.module';
import { WarehouseService } from './warehouse.service';
import { WarehouseListComponent } from './components/warehouse-list.component';
import { WarehouseNearestComponent } from './components/warehouse-nearest.component';
import { LayoutComponent } from '../layout/layout.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpAuthAddTokenInterceptor } from '../common/auth/interceptors/auth.http.addtoken.interceptor';
import { HttpErrorInterceptor } from '../common/auth/interceptors/common.http.error.interceptor';

@NgModule({
  imports: [
    CommonModule,
    MapsModule,
    WarehouseRoutingModule,
    NzPageHeaderModule,
    NzGridModule,
  ],
  exports: [],
  entryComponents:[LayoutComponent],
  declarations: [WarehouseComponent, WarehouseListComponent,WarehouseNearestComponent,LayoutComponent],
  providers: [WarehouseService,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpAuthAddTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
  ],
})
export class WarehouseModule {}
