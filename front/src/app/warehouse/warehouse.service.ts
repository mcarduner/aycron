import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import data from '../../assets/json/data.json';
import { HttpClientService } from '../common/services/http-client.service';
import { WarehouseAddModel } from './models/warehouse.list.mode';
@Injectable({ providedIn: 'root' })
export class WarehouseService {
  private basePath = 'admin/WareHouse';

  constructor(private http: HttpClientService) {}

  /**
   * get the states from a json
   * @returns
   */
  getStates(): Observable<any> {
    return of(data.states);
  }
  /**
   * get the states from a json
   * @returns
   */
  getCounty(stateId: string | number): Observable<any> {
    return of(data.counties.filter((c) => c.parent === stateId));
  }

  public save(data: WarehouseAddModel): Observable<any> {
    return (data.id || 0) > 0
      ? this.http.put(this.basePath, '', data)
      : this.http.post(this.basePath, '', data);
  }

  public getById(id: number | string): Observable<any> {
    return this.http.get(this.basePath, `${id}`);
  }

  getAllWarehouse(): Observable<any> {
    return this.http.post(this.basePath, 'list', {
      filter: '',
      pageSize: 1000,
      page: 0,
    });
  } //Filter and pagination on client

  getNearestWarehouse(
    count: number,
    location: google.maps.LatLng
  ): Observable<any> {
    return this.http.post(this.basePath, 'listLocation', {
      count: count,
      geo: location.toJSON(),
    });
  }

  delete(id: number | string): Observable<any> {
    return this.http.delete(this.basePath, `${id}`);
  }

  export(search: string) {
    return this.http.postClob(this.basePath, `listExcel`, 'list.xls', {
      filter: search,
      page: 0,
      pageSize: 1000,
    });
  }
}
