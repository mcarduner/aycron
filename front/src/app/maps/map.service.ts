import { makeBindingParser } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class MapService {
  geocoder: google.maps.Geocoder;
  direction: google.maps.DirectionsService;

  constructor() {
    this.geocoder = new google.maps.Geocoder();
    this.direction = new google.maps.DirectionsService();
  }

  private map!: google.maps.Map;

  private markers: google.maps.Marker[] = [];

  private polyline: google.maps.Polyline[] = [];

  private directions: google.maps.DirectionsRenderer[] = [];

  /**
   * Creates a new map inside of the given HTML container.
   *
   * @param el DIV element
   * @param mapOptions MapOptions object specification
   */
  public initMap(el: HTMLElement, mapOptions: any): void {
    this.map = new google.maps.Map(el, mapOptions);
    this.resize();
    google.maps.event.addDomListener(window, 'resize', () => this.resize());
  }

  public get Map(): google.maps.Map {
    return this.map;
  }

  /**
   * get the lat and lng of an address
   * @param address
   * @returns
   */
  getCodeAddress(
    address: string
  ): Observable<google.maps.GeocoderResult[] | null> {
    return Observable.create(
      (observer: Observer<google.maps.GeocoderResult[] | null>) => {
        this.geocoder.geocode(
          { address: address },
          (
            results: google.maps.GeocoderResult[] | null,
            status: google.maps.GeocoderStatus | null
          ) => {
            if (status === google.maps.GeocoderStatus.OK) {
              observer.next(results);
              observer.complete();
            } else {
              console.log('Geocoding service: error : ' + status);
              observer.error(status);
            }
          }
        );
      }
    );
  }

  getDirection(
    request: any
  ): Observable<google.maps.DirectionsResult[] | null> {
    return Observable.create(
      (observer: Observer<google.maps.DirectionsResult[] | null>) => {
        this.direction.route(
          request,
          (
            results: any | null,
            status: google.maps.DirectionsStatus | null
          ) => {
            if (status == google.maps.DirectionsStatus.OK) {
              observer.next(results);
              observer.complete();
            } else {
              console.log('Geocoding service: error : ' + status);
              observer.error(status);
            }
          }
        );
      }
    );
  }

  /**
   * Center map
   * @param latLng
   */
  setCenter(latLng: google.maps.LatLng): void {
    if (this.map != null && latLng != null) {
      this.map.panTo(latLng);
    }
  }

  setDirection(request: any, panel: HTMLElement | null) {
    let directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setDirections(request);
    directionsDisplay.setPanel(panel);
    directionsDisplay.setMap(this.map);
    this.directions.push(directionsDisplay);
  }

  /**
   *
   * @param zoom
   */
  setZoom(zoom: number): void {
    if (this.map != null) {
      this.map.setZoom(zoom);
    }
  }

  setBounds(bounds: google.maps.LatLngBounds): void {
    if (this.map != null) {
      this.map.fitBounds(bounds);
      this.map.panToBounds(bounds);
    }
  }

  setPolyline(data: any, color: string) {
    const polyline = new google.maps.Polyline({
      path: data,
      geodesic: true,
      strokeColor: color,
      strokeOpacity: 0.8,
      strokeWeight: 2,
    });

    polyline.setMap(this.map);

    this.polyline.push(polyline);
  }

  /**
   * Adds a marker.
   *
   * @param latLng Marker position
   * @param title Tooltip
   * @param contentString InfoWindow' content
   */
  addMarker(
    latLng: google.maps.LatLng,
    title?: string,
    contentString?: string,
    icon?: string
  ): void {
    if (this.map != null && latLng != null) {
      // Creates the marker.
      const marker: google.maps.Marker = new google.maps.Marker({
        position: latLng,
        title: title,
      });

      if (icon !== undefined) {
        marker.setIcon(icon);
      }

      marker.setMap(this.map);

      if (contentString != null) {
        const width: number = this.map.getDiv().clientWidth;
        const infoWindow: google.maps.InfoWindow = new google.maps.InfoWindow({
          content: contentString,
          maxWidth: width,
        });
        marker.addListener('click', () => {
          infoWindow.open(this.map, marker);
        });
      }
      this.markers.push(marker);
    }
  }

  deleteMarkers(): void {
    for (let i: number = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers = [];
  }


  deleteDirections(): void {
    for (let i: number = 0; i < this.directions.length; i++) {
      this.directions[i].setMap(null);
    }
    this.directions = [];
  }

  deletePolyline(): void {
    for (let i: number = 0; i < this.polyline.length; i++) {
      this.polyline[i].setMap(null);
    }
    this.polyline = [];
  }

  public resize(): void {
    const latLng: any = this.map.getCenter();
    setTimeout(() => {
      google.maps.event.trigger(this.map, 'resize');
      this.map.setCenter(latLng);
    });
  }

  getCurrentPosition(): Observable<any> {
    return Observable.create((observer: Observer<any>) => {
      navigator.geolocation.getCurrentPosition(
        (position: any) => {
          observer.next(position);
          observer.complete();
        },
        (error: any) => {
          console.log('Geolocation service: ' + error.message);
          observer.error(error);
        }
      );
    });
  }
}
