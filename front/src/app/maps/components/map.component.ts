import {
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import { MapService } from '../map.service';

@Component({
  selector: 'wh-map',
  templateUrl: './map.component.html',
})
export class MapComponent implements OnInit {
  /**
   * Center of Map
   */
  center = new google.maps.LatLng(-32.88309505579885, -68.68725799272775);

  @Output('onChange') onChange: EventEmitter<google.maps.LatLng | null> =
    new EventEmitter();

  latLngSelected: google.maps.LatLng | null = null;

  /**
   * ctro
   * @param service
   * @param elementRef
   */
  constructor(private service: MapService, private elementRef: ElementRef) {}

  /**
   *
   */
  ngOnInit() {
    this.getCurrentPosition();
  }

  /**
   * get the current position with the browser
   */
  getCurrentPosition(): void {
    // if (navigator.geolocation) {
    //   this.service
    //     .getCurrentPosition()
    //     .subscribe((position: any) => {
    //       if (
    //         this.center.lat() != position.coords.latitude &&
    //         this.center.lng() != position.coords.longitude
    //       ) {
    //         this.center = new google.maps.LatLng(
    //           position.coords.latitude,
    //           position.coords.longitude
    //         );
    //       }
    //     })
    //     .add(() => this.createMap());
    // } else {
    this.createMap();
    //}
  }

  /**
   * create the map
   */
  createMap(): void {
    const el: HTMLElement = this.elementRef.nativeElement.querySelector('#map');
    this.service.initMap(el, {
      center: this.center,
      zoom: 15,
    });
  }

  /**
   * set a address
   * @param address
   * @param title
   * @param content
   */
  public setAddress(address: string, title: string, content: string) {
    this.service
      .getCodeAddress(address)
      .subscribe((r: google.maps.GeocoderResult[] | null) => {
        if (r && r.length > 0) {
          var p = r[0];
          this.setMarker(p.geometry.location, title, content);
        }
      });
  }

  public setDirections(reques: any, panel: HTMLElement | null) {
    this.service.getDirection(reques).subscribe((r: any[] | null) => {
      this.service.setDirection(r, panel);
    });
  }

  public deleteMarkers() {
    this.service.deleteMarkers();
  }

  public deleteDirections() {
    this.service.deleteDirections();
  }

  /**
   * set a marker
   * @param latLng
   * @param title
   * @param content
   */
  public setMarker(
    latLng: google.maps.LatLng,
    title: string,
    content: string
  ): void {
    this.service.deleteMarkers();
    this.service.addMarker(latLng, title, content);
    this.service.setCenter(latLng);
    this.service.setZoom(18);
    this.onChange.emit(latLng);
    this.latLngSelected = latLng;
  }

  /**
   * set a marker
   * @param latLng
   * @param title
   * @param content
   */
  public setMultiplesMarker(
    data: {
      latLng: google.maps.LatLng;
      title: string;
      content: string;
      icon?: string;
    }[]
  ): void {
    let bounds = new google.maps.LatLngBounds();
    data.forEach((i) => {
      bounds.extend(i.latLng);
      this.service.addMarker(i.latLng, i.title, i.content, i.icon);
    });

    this.service.setBounds(bounds);
  }

  public setPolyline(data: any, color: string = '#FF0000') {
    this.service.setPolyline(data, color);
  }

  public deletePolyline() {
    this.service.deletePolyline();
  }

  public get currentLatLng(): google.maps.LatLng | null {
    return this.latLngSelected;
  }
}
