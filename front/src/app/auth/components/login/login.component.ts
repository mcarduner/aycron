import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AuthService } from 'src/app/common/auth/auth.service';
import { BaseComponent } from 'src/app/common/components/base.component';
import { LoginService } from '../../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
})
export class LoginComponent extends BaseComponent implements OnInit {
  isLoading = false;
  form!: FormGroup;

  constructor(
    el: ElementRef,
    router: Router,
    messages: NzMessageService,
    private fb: FormBuilder,
    private loginService: LoginService,
    private authService: AuthService
  ) {
    super(el, messages, router);
  }

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    this.form = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  login() {
    if (!this.isValidForm(this.form)) return;
    let user = this.getModel();
    this.isLoading = true;

    this.loginService
      .login(user)
      .subscribe((r) => {
        this.authService.token = r.token;
        this.authService.currentUser = r.user;
        this.navigateToHome();
      })
      .add(() => (this.isLoading = false));
  }

  getModel() {
    return {
      username: this.form.controls['username'].value,
      password: this.form.controls['password'].value,
    };
  }

  private navigateToHome() {
    this.router?.navigate(['/home/list']);
  }
}
