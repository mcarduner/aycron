import { NgModule } from '@angular/core';
import { CommonModule } from '../common/common.module';
import { LoginComponent } from './components/login/login.component';

import { LoginRoutingModule } from './login-routing.module';
import { LoginService } from './login.service';
import { CommonModule as Common } from '@angular/common';
@NgModule({
  declarations: [LoginComponent],
  imports: [Common, CommonModule, LoginRoutingModule],
  providers: [LoginService],
})
export class LoginModule {}
