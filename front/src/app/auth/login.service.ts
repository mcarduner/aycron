import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from '../common/services/http-client.service';
import { LoginDto } from './models/login-dto.model';

@Injectable()
export class LoginService {

  private basePath = 'auth';

  constructor(private http: HttpClientService) {}

  public login(data: LoginDto): Observable<any> {
    return this.http.post(this.basePath, 'login', data);
  }

}
