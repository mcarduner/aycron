import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../common/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './layout.component.html',
})
export class LayoutComponent implements OnInit {
  /**
   * ctro
   */
  constructor(private authService: AuthService, private router: Router) {}

  /**
   * init event angular
   */
  ngOnInit() {}

  logout() {
    this.authService.logout();
    this.router.navigate(['/auth/login']);
  }
}
