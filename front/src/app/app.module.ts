import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { WarehouseModule } from './warehouse/warehouse.module';
import en from '@angular/common/locales/en';
import { registerLocaleData } from '@angular/common';
import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';
import { AppComponent } from './app.component';
import { CommonModule } from './common/common.module';
import { CommonModule as Common } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpAuthAddTokenInterceptor } from './common/auth/interceptors/auth.http.addtoken.interceptor';
import { HttpErrorInterceptor } from './common/auth/interceptors/common.http.error.interceptor';
import { NzConfig, NZ_CONFIG } from 'ng-zorro-antd/core/config';

registerLocaleData(en);
const ngZorroConfig: NzConfig = {
  message: { nzMaxStack: 1, nzDuration: 7000 },
};
@NgModule({
  declarations: [AppComponent],
  imports: [
    WarehouseModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    Common,
    CommonModule,
    NzLayoutModule,
    NzMenuModule,
    NzAvatarModule,
    NzIconModule,
  ],
  bootstrap: [AppComponent],
  providers: [

    { provide: NZ_I18N, useValue: en_US },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpAuthAddTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    { provide: NZ_CONFIG, useValue: ngZorroConfig }
  ],
})
export class AppModule {}
