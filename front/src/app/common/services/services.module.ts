import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientService } from './http-client.service';

@NgModule({
  declarations: [],
  providers: [HttpClientService]
})
export class ServicesModule { }
