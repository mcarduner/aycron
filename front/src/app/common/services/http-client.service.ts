import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class HttpClientService {
    public ENDPOINT: string = `${environment.api.url}`;
    public VERSION: string = `${environment.api.ver}`;

    constructor(public httpClient: HttpClient) { }

    /** @description Realiza un get a la api */
    get(
        basePath: string,
        path: string,
        showSpinner: boolean = false
    ): Observable<any> {
        return this.httpClient.get(
        `${this.ENDPOINT}/api/v${this.VERSION}/${basePath}/${path}`
        );
    }

    /** @description: Realiza un post a la api */
    post(
        basePath: string,
        path: string,
        data: any,
        showSpinner: boolean = false
    ): Observable<any> {
        return this.httpClient.post(
        `${this.ENDPOINT}/api/v${this.VERSION}/${basePath}/${path}`,
        data
        );
    }

    /** @description Realiza un put a la api */
    put(
        basePath: string,
        path: string,
        data: any,
        showSpinner: boolean = false
    ): Observable<any> {
        return this.httpClient.put(
        `${this.ENDPOINT}/api/v${this.VERSION}/${basePath}/${path}`,
        data
        );
    }

    /** @description Realiza un patch a la api */
    patch(
        basePath: string,
        path: string,
        data: any,
        showSpinner: boolean = false
    ): Observable<any> {
        return this.httpClient.patch(
        `${this.ENDPOINT}/api/v${this.VERSION}/${basePath}/${path}`,
        data
        );
    }

    /** @description Realiza un delete a la api */
    delete(
        basePath: string,
        path: string,
        data?: any,
        showSpinner: boolean = false
    ): Observable<any> {
        return data
        ? this.httpClient.request(
            'delete',
            `${this.ENDPOINT}/api/v${this.VERSION}/${basePath}/${path}`,
            { body: data }
            )
        : this.httpClient.delete(
            `${this.ENDPOINT}/api/v${this.VERSION}/${basePath}/${path}`
            );
    }

    postClob(
      basePath: string,
      path: string,
      filename: string,
      data: any,
      download: boolean = true
    ): Observable<any> {
      return this.httpClient
        .post(  `${this.ENDPOINT}/api/v${this.VERSION}/${basePath}/${path}`, data, {
          responseType: 'blob' as 'json',
        })
        .pipe(
          map((response: any) => {
            const dataType = response.type;
            const binaryData = [];
            binaryData.push(response);
            const downloadLink = document.createElement('a');
            var objectUrl = window.URL.createObjectURL(
              new Blob(binaryData, { type: dataType })
            );
            downloadLink.href = objectUrl;
            if (download) {
              downloadLink.download = filename;
            } else {
              downloadLink.target = '_blank';
            }
            downloadLink.click();
            URL.revokeObjectURL(objectUrl);
          })
        );
    }
}
