import { Injectable } from '@angular/core';
import { AuthUserModel } from './models/auth-user.model';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  /** @description Usuario de la aplicacion @type AuthUserModel */
  private user: AuthUserModel;

  constructor() {
    this.user = JSON.parse(sessionStorage.getItem('auth-user')  || '{}') as AuthUserModel;
  }

  public set token(token: string) {
    sessionStorage.setItem('token', JSON.stringify(token));
  }
  public get token(): string {
    return JSON.parse(sessionStorage.getItem('token')|| '{}');
  }

  public set currentUser(user: AuthUserModel) {
    sessionStorage.setItem('auth-user', JSON.stringify(user));
    this.user = user;
  }
  public get currentUser(): AuthUserModel {
    return this.user;
  }

  /** @description Cierra sesion usuario */
  logout() {
    sessionStorage.removeItem('auth-user');
    sessionStorage.removeItem('token')
  }

}
