import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpErrorResponse,
} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private message: NzMessageService,private router: Router) {}

  /**
   * @description Intercepta todos los HttpErrorResponse del sistema
   * y muestra el mensaje correspondiente al usuario segun su status.
   * @param req
   * @param next
   */
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(
      catchError((errorResponse: any) => {
        if (errorResponse.status === 401 || errorResponse.status === 403) {
          this.router.navigate(['auth/login']);
        } else {
          if (errorResponse.status === 400) {
            this.message.create('error', errorResponse.error);
          } else {
            this.message.create(
              'error',
              'An unexpected error has occurred. Please try again or request support.'
            );
          }
        }
        return throwError(() => new HttpErrorResponse(errorResponse));
      })
    );
  }
}
