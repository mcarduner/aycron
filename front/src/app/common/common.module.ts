import { NgModule } from '@angular/core';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule as CommonModuleAngular } from '@angular/common';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { ServicesModule } from './services/services.module';

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { AuthGuard } from './auth/auth.guardian';
@NgModule({
  imports: [
    ReactiveFormsModule,
    FormsModule,
    NzInputModule,
    NzFormModule,
    NzSelectModule,
    NzSpaceModule,
    NzButtonModule,
    NzIconModule,
    NzCardModule,
    NzMessageModule,
    NzTableModule,
    NzCollapseModule,
    NzDrawerModule,
    NzSpinModule,
    NzInputNumberModule,
    ServicesModule,
    NzLayoutModule,
    NzMenuModule,
    NzAvatarModule,
    CommonModuleAngular,
    NzPopconfirmModule,
    NzToolTipModule
  ],
  exports: [
    ReactiveFormsModule,
    FormsModule,
    NzInputModule,
    NzFormModule,
    NzSelectModule,
    NzSpaceModule,
    NzButtonModule,
    NzIconModule,
    NzCardModule,
    NzMessageModule,
    NzTableModule,
    NzCollapseModule,
    NzDrawerModule,
    NzSpinModule,
    NzInputNumberModule,
    ServicesModule,
    NzLayoutModule,
    NzMenuModule,
    NzAvatarModule,
    CommonModuleAngular,
    NzPopconfirmModule,
    NzToolTipModule
  ],
  declarations: [],
  providers: [AuthGuard],
})
export class CommonModule {}
