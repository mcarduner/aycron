import { Component, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService, NzMessageDataOptions } from 'ng-zorro-antd/message';

@Component({
  template: '',
})
export class BaseComponent {
  private options: NzMessageDataOptions = {nzDuration: 5000};

  get router(): Router | undefined {
    return this._router;
  }

  constructor(
    private el?: ElementRef,
    private message?: NzMessageService,
    private _router?: Router
  ) { }

  public isValidForm(
    form: FormGroup,
    showMessageError: boolean = false,
    messageError?: string
    ): boolean{
      for (const key in form.controls){
        if (form.controls[key].invalid){
          const invalidControl = this.el?.nativeElement.querySelector(
            '[formcontrolname="'+ key + '"]'
          );

          if(invalidControl){
            invalidControl.focus();
          }

          this.markErrorsInControls(form);

          if (showMessageError) {
            const message =
              messageError ?? 'Please check each field of the form.';
            this.showMessageError(message);
          }

          return false;
        }
      }
      return true;
  }

  private markErrorsInControls(form: FormGroup) {
    for (const key in form.controls) {
      form.controls[key].markAsDirty();
      form.controls[key].updateValueAndValidity();
    }
  }

  public showMessageError(message: string , options?: NzMessageDataOptions) {
    this.message!.create('error', message, options ?? this.options);
  }

  public showMessageSuccess(message: string, options?: NzMessageDataOptions) {
    this.message!.create('success', message, options ?? this.options);
  }

  public redirectTo(url: string) {
    this._router!.navigateByUrl(url);
  }
}
