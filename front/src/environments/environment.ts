export const environment = {
  production: false,
  api: {
    url: 'https://localhost:5001',
    ver: '1',
  },
  country: 'Argentina',
};
